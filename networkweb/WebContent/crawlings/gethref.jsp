<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	function pop(url){
		window.open(url);
	}
</script>
</head>
<body>
	<a href="gethref">a태그 가져오기</a>
	<ul>
		<c:forEach items="${list}" var="a">
			<li><a href="javascript:window.open('${a.attr('href') }','','width=800,height=600')">${a.attr("href") }</a></li>
		</c:forEach>
	</ul>
	

</body>
</html>