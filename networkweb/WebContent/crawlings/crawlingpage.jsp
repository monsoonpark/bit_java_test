<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<c:url var="crawlcss" value="/css/crawlcss.css" />
<link href="${crawlcss }" rel="stylesheet" type="text/css">
<title>Crawling Test</title>
</head>
<body>
<h1>Crawling Test</h1>

<div class="container" style="margin:0px; width:100%;">
	<input type="hidden" id="bsort" name="bsort" value="1" readonly/>
	<div class="tabbable tabs-left">
		<!-- 왼쪽과 오른쪽 탭에만 필요 -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab">가장 많이 본 기사</a></li>
			<li><a href="#tab2" data-toggle="tab">실시간 검색어</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1">	<!-- 기사 -->
					<div class="container" style="margin: 0px; width: 100%;">
						<input type="hidden" id="bsort" name="bsort" value="1" readonly />
						<div class="tabbable" id="tabbable">
							<!-- 왼쪽과 오른쪽 탭에만 필요 -->
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab01" data-toggle="tab">네이버</a></li>
								<li><a href="#tab03" data-toggle="tab">다음</a></li>
								<li><a href="#tab04" data-toggle="tab">네이트</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab01">
									<!-- 네이버 -->
									<ul>
										<c:forEach items="${as1}" var="a">
											<li>${a.text() }</li>
										</c:forEach>
									</ul>
								</div>

								<div class="tab-pane fade" id="tab03">
									<!-- 다음 -->
									<ul>
										<c:forEach items="${as2}" var="a">
											<li>${a.text() }</li>
										</c:forEach>
									</ul>
								</div>

								<div class="tab-pane fade" id="tab04">
									<!-- 네이트 -->
									<ul>
										<c:forEach items="${as3}" var="a">
											<li>${a.text() }</li>
										</c:forEach>
										<c:forEach items="${as3_2}" var="a">
											<li>${a.text() }</li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>
					</div>


				</div>
			
			<div class="tab-pane fade" id="tab2">	<!-- 검색어 -->
				<div class="container" style="margin: 0px; width: 100%;">
						<input type="hidden" id="bsort" name="bsort" value="1" readonly />
						<div class="tabbable" id="tabbable">
							<!-- 왼쪽과 오른쪽 탭에만 필요 -->
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab001" data-toggle="tab">다음</a></li>
								<li><a href="#tab003" data-toggle="tab">네이트</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab001">
									<!-- 네이버 -->
									<ul>
										<c:forEach items="${as22}" var="a">
											<li>${a.text() }</li>
										</c:forEach>
									</ul>
								</div>

								<div class="tab-pane fade" id="tab003">
									<!-- 다음 -->
									<ul>
										<c:forEach items="${as33}" var="a">
											<li>${a.text() }</li>
										</c:forEach>
									</ul>
								</div>

							</div>
						</div>
					</div>
			</div>
			
		</div>
	</div>
</div>

</body>
</html>