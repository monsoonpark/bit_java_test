package net;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressEx {
	public static void main(String[] args) throws UnknownHostException{
		InetAddress iaddr=InetAddress.getLocalHost();
		System.out.printf("호스트 이름: %s %n",iaddr.getHostName());
		System.out.printf("호스트 IP: %s %n",iaddr.getHostAddress());
		iaddr=InetAddress.getByName("www.oracle.com");
		System.out.printf("호스트 이름: %s %n",iaddr.getHostName());
		System.out.printf("호스트 IP: %s %n",iaddr.getHostAddress());
		
		InetAddress sw[]=InetAddress.getAllByName("www.daum.net");
		for(InetAddress temp_sw:sw) {
			System.out.printf("호스트 이름: %s %n",temp_sw.getHostName());
			System.out.printf("호스트 IP: %s %n",temp_sw.getHostAddress());
			
		}
		
		
	}
}
