package crawlings;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bit.nets.Crawlings;

public class GetText_test {

	public static void main(String[] args) throws IOException {
		//초기
		Document d=Crawlings.getDoc("http://news.naver.com/");
		//href값 가져오기
		Elements as=d.select(".section_list_ranking a");
		//href값 출력
		for (Element a : as) {
			System.out.println(a.text());
		}

	}

}
