package crawlings;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Servlet implementation class getImg
 */
@WebServlet("/crawlings/gethref")
public class getHref extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getHref() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//초기
		ArrayList<String> ars=new ArrayList<>();
		String url="http://news.naver.com/";
		//html 문서 객체
		Document d=Jsoup.connect(url).get();
		//href값 가져오기
		Elements as=d.select("a");
		Elements list=new Elements();
		//href값 출력
		for (Element a : as) {
			if(a.toString().contains("http")) {
				//System.out.println(a.attr("href"));
//				ars.add(a.attr("href"));
				ars.add(a.attr("href").toString().replaceAll(" ", ""));
				list.add(a);
			}
		}
		//setAttribute
		request.setAttribute("ars", ars);
		request.setAttribute("list", list);
		request.getRequestDispatcher("gethref.jsp").forward(request, response);
		
	}
	
	
	

}
