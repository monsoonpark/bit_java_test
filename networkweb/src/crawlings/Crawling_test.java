package crawlings;

import java.io.IOException;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawling_test {
	//멤버변수
	
	public static void main(String[] args) throws IOException {
		//초기
		String url="http://news.naver.com/";
		//html 문서 객체
		Document d=Jsoup.connect(url).get();
		//특정 Element 객체
		Elements els=d.select("ul div.newsnow_tx_inner");
		//객체 출력
		for (Element e : els) {
			System.out.println(e);
		}
		
	}

}
