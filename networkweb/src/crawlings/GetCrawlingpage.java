package crawlings;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import bit.nets.Crawlings;

/**
 * Servlet implementation class getImg
 */
@WebServlet("/crawlings/getcrawlingpage")
public class GetCrawlingpage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCrawlingpage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//초기
		//html 문서 객체
		Document d1=Crawlings.getDoc("http://news.naver.com/");
		Document d2=Crawlings.getDoc("http://media.daum.net/ranking/popular/");
		Document d3=Crawlings.getDoc("http://news.nate.com/rank/pop");
		Document d22=Crawlings.getDoc("https://www.daum.net/");
		Document d33=Crawlings.getDoc("http://www.nate.com/?f=bi");
		
		
		
		//href값 가져오기
		Elements as1=d1.select(".section_list_ranking a");
		Elements as2=d2.select(".cont_thumb strong a");
		Elements as3=d3.select(".tit");
		Elements as3_2=d3.select(".postRankSubject ul li a");
		
		Elements as22=d22.select(".roll_txt .rank_cont .txt_issue a");
		Elements as33=d33.select(".kwd_list a");
		
		
		
		
		//href값 출력
		for (Element a : as3) {
		
		}
		//setAttribute
		request.setAttribute("as1", as1);
		request.setAttribute("as2", as2);
		request.setAttribute("as3", as3);
		request.setAttribute("as3_2", as3_2);
		
		request.setAttribute("as22", as22);
		request.setAttribute("as33", as33);
		
		request.getRequestDispatcher("crawlingpage.jsp").forward(request, response);
		
	}
	
	
	

}
