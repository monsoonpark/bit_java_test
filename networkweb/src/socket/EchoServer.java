package socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
	private BufferedReader bufferR;
	private BufferedWriter bufferW;
	private InputStream is;
	private OutputStream os;
	private ServerSocket serverS;
	
	public EchoServer(int port) {
		try {
			serverS=new ServerSocket(port);
		}catch(IOException E) {
			E.printStackTrace();
			System.exit(0);
		}
		
		while(true) {
			System.out.println("클라이언트 요청 대기중");
			try {
				Socket tcpSocket=serverS.accept();
				System.out.println("클라이언트 IP: "+tcpSocket.getInetAddress().getHostAddress());
				is=tcpSocket.getInputStream();
				os=tcpSocket.getOutputStream();
				bufferR=new BufferedReader(new InputStreamReader(is));
				bufferW=new BufferedWriter(new OutputStreamWriter(os));
				String message=bufferR.readLine();
				System.out.println("수신 메시지:"+message);
				message+=System.getProperty("line.separator");
				bufferW.write(message);
				bufferW.flush();
				bufferR.close();
				bufferW.close();
				tcpSocket.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String[] args) {
		new EchoServer(3000);
	}

}
