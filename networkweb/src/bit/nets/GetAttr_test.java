package bit.nets;

import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetAttr_test {

	public static void main(String[] args) {
		//초기
		Document d=Crawlings.getDoc("http://news.naver.com/");
		//href값 가져오기
		Elements as=d.select("a");
		//href값 출력
		for (Element a : as) {
			if(!a.attr("href").contains("#")) {
				System.out.println(a.attr("href"));
			}
		}

	}

}
