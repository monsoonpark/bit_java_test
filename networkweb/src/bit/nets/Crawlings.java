package bit.nets;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Crawlings {
	
	//URL을 전달받아 Document를 리턴
	public static Document getDoc(String url) {
		//html 문서 객체
		Document d=null;
		try {
			d = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return d;
	}
	
	//
}
