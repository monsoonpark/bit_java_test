<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
/* Set height of the grid so .sidenav can be 100% (adjust if needed) */
.row.content {
 height: 1500px
}

/* Set gray background color and 100% height */
.sidenav {
 background-color: #f1f1f1;
 height: 100%;
}

/* Set black background color, white text and some padding */
footer {
 background-color: #555;
 color: white;
 padding: 15px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
 .sidenav {
  height: auto;
  padding: 15px;
 }
 .row.content {
  height: auto;
 }
}
</style>
</head>
<body>

 <div class="container-fluid">
  <div class="row content">
   <div class="col-sm-3 sidenav">
    <h4>John's Blog</h4>
    <ul class="nav nav-pills nav-stacked">
     <li class="active"><a href="#section1">Home</a></li>
     <li><a href="#section2">Friends</a></li>
     <li><a href="#section3">Family</a></li>
     <li><a href="#section3">Photos</a></li>
    </ul>
    <br>
    <div class="input-group">
     <input type="text" class="form-control" placeholder="Search Blog..">
     <span class="input-group-btn">
      <button class="btn btn-default" type="button">
       <span class="glyphicon glyphicon-search"></span>
      </button>
     </span>
    </div>
   </div>

   <div class="col-sm-9">
    <h4>
     <small>RECENT POSTS</small>
    </h4>
    <hr>
    <h2>Nested Media Objects</h2>
    <p>Media objects can also be nested (a media object inside a
     media object):</p>
    <br>
    <div class="media">
     <div class="media-left">
      <img src="img_avatar1.png" class="media-object"
       style="width: 45px">
     </div>
     <div class="media-body">
      <h4 class="media-heading">
       John Doe <small><i>Posted on February 19, 2016</i></small>
      </h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
       sed do eiusmod tempor incididunt ut labore et dolore magna
       aliqua.</p>

      <!-- Nested media object -->
      <div class="media">
       <div class="media-left">
        <img src="img_avatar2.png" class="media-object"
         style="width: 45px">
       </div>
       <div class="media-body">
        <h4 class="media-heading">
         John Doe <small><i>Posted on February 19, 2016</i></small>
        </h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
         sed do eiusmod tempor incididunt ut labore et dolore magna
         aliqua.</p>

        <!-- Nested media object -->
        <div class="media">
         <div class="media-left">
          <img src="img_avatar3.png" class="media-object"
           style="width: 45px">
         </div>
         <div class="media-body">
          <h4 class="media-heading">
           John Doe <small><i>Posted on February 19, 2016</i></small>
          </h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing
           elit, sed do eiusmod tempor incididunt ut labore et dolore
           magna aliqua.</p>
         </div>
        </div>

       </div>
      </div>

     </div>
    </div>
    <br> <br>


    <h4>
     <small>FORMS</small>
    </h4>
    <hr>
    <form class="form-horizontal" action="/action_page.php">
     <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
       <input type="email" class="form-control" id="email"
        placeholder="Enter email">
      </div>
     </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-10">
       <input type="password" class="form-control" id="pwd"
        placeholder="Enter password">
      </div>
     </div>
     <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
       <div class="checkbox">
        <label><input type="checkbox"> Remember me</label>
       </div>
      </div>
     </div>
     <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
       <button type="submit" class="btn btn-default">Submit</button>
      </div>
     </div>
    </form>
    <br> <br>


    <h4>
     <small>Image Gallery</small>
    </h4>
    <hr>
    <div class="row">
     <div class="col-md-4">
      <div class="thumbnail">
       <a href="lights.jpg"> <img src="lights.jpg" alt="Lights"
        style="width: 100%">
        <div class="caption">
         <p>Lorem ipsum...</p>
        </div>
       </a>
      </div>
     </div>
     <div class="col-md-4">
      <div class="thumbnail">
       <a href="nature.jpg"> <img src="nature.jpg" alt="Nature"
        style="width: 100%">
        <div class="caption">
         <p>Lorem ipsum...</p>
        </div>
       </a>
      </div>
     </div>
     <div class="col-md-4">
      <div class="thumbnail">
       <a href="fjords.jpg"> <img src="fjords.jpg" alt="Fjords"
        style="width: 100%">
        <div class="caption">
         <p>Lorem ipsum...</p>
        </div>
       </a>
      </div>
     </div>
    </div>
    <br> <br>



    <h4>
     <small>TABLES</small>
    </h4>
    <hr>
    <p>
    <table class="table">
     <thead>
      <tr>
       <th>Firstname</th>
       <th>Lastname</th>
       <th>Email</th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td>Default</td>
       <td>Defaultson</td>
       <td>def@somemail.com</td>
      </tr>
      <tr class="success">
       <td>Success</td>
       <td>Doe</td>
       <td>john@example.com</td>
      </tr>
      <tr class="danger">
       <td>Danger</td>
       <td>Moe</td>
       <td>mary@example.com</td>
      </tr>
      <tr class="info">
       <td>Info</td>
       <td>Dooley</td>
       <td>july@example.com</td>
      </tr>
      <tr class="warning">
       <td>Warning</td>
       <td>Refs</td>
       <td>bo@example.com</td>
      </tr>
      <tr class="active">
       <td>Active</td>
       <td>Activeson</td>
       <td>act@example.com</td>
      </tr>
     </tbody>
    </table>
    </p>

    <br> <br>

    <h4>
     <small>RECENT POSTS</small>
    </h4>
    <hr>
    <h2>I Love Food</h2>
    <h5>
     <span class="glyphicon glyphicon-time"></span> Post by Jane Dane,
     Sep 27, 2015.
    </h5>
    <h5>
     <span class="label label-danger">Food</span> <span
      class="label label-primary">Ipsum</span>
    </h5>
    <br>
    <p>Food is my passion. Lorem ipsum dolor sit amet, consectetur
     adipiscing elit, sed do eiusmod tempor incididunt ut labore et
     dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
     exercitation ullamco laboris nisi ut aliquip ex ea commodo
     consequat. Excepteur sint occaecat cupidatat non proident, sunt in
     culpa qui officia deserunt mollit anim id est laborum consectetur
     adipiscing elit, sed do eiusmod tempor incididunt ut labore et
     dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
     exercitation ullamco laboris nisi ut aliquip ex ea commodo
     consequat.</p>
    <br> <br>

    <h4>
     <small>RECENT POSTS</small>
    </h4>
    <hr>
    <h2>Officially Blogging</h2>
    <h5>
     <span class="glyphicon glyphicon-time"></span> Post by John Doe,
     Sep 24, 2015.
    </h5>
    <h5>
     <span class="label label-success">Lorem</span>
    </h5>
    <br>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
     do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
     enim ad minim veniam, quis nostrud exercitation ullamco laboris
     nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat
     cupidatat non proident, sunt in culpa qui officia deserunt mollit
     anim id est laborum consectetur adipiscing elit, sed do eiusmod
     tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
     minim veniam, quis nostrud exercitation ullamco laboris nisi ut
     aliquip ex ea commodo consequat.</p>
    <hr>

    <h4>Leave a Comment:</h4>
    <form role="form">
     <div class="form-group">
      <textarea class="form-control" rows="3" required></textarea>
     </div>
     <button type="submit" class="btn btn-success">Submit</button>
    </form>
    <br> <br>

    <p>
     <span class="badge">2</span> Comments:
    </p>
    <br>

    <div class="row">
     <div class="col-sm-2 text-center">
      <img src="bandmember.jpg" class="img-circle" height="65"
       width="65" alt="Avatar">
     </div>
     <div class="col-sm-10">
      <h4>
       Anja <small>Sep 29, 2015, 9:12 PM</small>
      </h4>
      <p>Keep up the GREAT work! I am cheering for you!! Lorem ipsum
       dolor sit amet, consectetur adipiscing elit, sed do eiusmod
       tempor incididunt ut labore et dolore magna aliqua.</p>
      <br>
     </div>
     <div class="col-sm-2 text-center">
      <img src="bird.jpg" class="img-circle" height="65" width="65"
       alt="Avatar">
     </div>
     <div class="col-sm-10">
      <h4>
       John Row <small>Sep 25, 2015, 8:25 PM</small>
      </h4>
      <p>I am so happy for you man! Finally. I am looking forward to
       read about your trendy life. Lorem ipsum dolor sit amet,
       consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua.</p>
      <br>
      <p>
       <span class="badge">1</span> Comment:
      </p>
      <br>
      <div class="row">
       <div class="col-sm-2 text-center">
        <img src="bird.jpg" class="img-circle" height="65" width="65"
         alt="Avatar">
       </div>
       <div class="col-xs-10">
        <h4>
         Nested Bro <small>Sep 25, 2015, 8:28 PM</small>
        </h4>
        <p>Me too! WOW!</p>
        <br>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>

 <footer class="container-fluid">
  <p>Footer Text</p>
 </footer>

</body>
</html>