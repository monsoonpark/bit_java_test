package filter;

import java.util.Date;

public class SessionLog {
	//멤버변수
	int c_id;
	String c_sessid;
	String c_ip;
	int c_sess;
	Date c_date;
	//
	public SessionLog(int c_id, String c_sessid, String c_ip, int c_sess, Date c_date) {
		super();
		this.c_id = c_id;
		this.c_sessid = c_sessid;
		this.c_ip = c_ip;
		this.c_sess = c_sess;
		this.c_date = c_date;
	}
	public SessionLog() {
		super();
	}
	//
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public String getC_sessid() {
		return c_sessid;
	}
	public void setC_sessid(String c_sessid) {
		this.c_sessid = c_sessid;
	}
	public String getC_ip() {
		return c_ip;
	}
	public void setC_ip(String c_ip) {
		this.c_ip = c_ip;
	}
	public int getC_sess() {
		return c_sess;
	}
	public void setC_sess(int c_sess) {
		this.c_sess = c_sess;
	}
	public Date getC_date() {
		return c_date;
	}
	public void setC_date(Date c_date) {
		this.c_date = c_date;
	}
	
	
	
	
	
}
