package filter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import sun.security.jca.GetInstance;
import the.bits.Bitdb;

public class GlobalSessionDao {
	private static GlobalSessionDao dao;
	
	static {
		dao=new GlobalSessionDao();
	}
	private GlobalSessionDao() {}
	
	//getInstance();
	public static GlobalSessionDao getInstance() {
		return dao;
	}
	
	//insertLog
	public void insertLog(SessionLog log) {
		String sql = String.format("INSERT INTO test_counter (c_id, c_sessid, c_ip, c_sess) " + 
			    "VALUES (SEQ_test_counter.NEXTVAL, '%s', '%s', %s) "
			    , log.getC_sessid(), log.getC_ip(), log.getC_sess());
		Bitdb.execute(sql);
	}
	//카운터 가져오기
	public int getCount() {
		String sql="select count(*) from test_counter";
		ResultSet rs=Bitdb.executeQuery(sql);
		
		try {
			if(rs.next()) {
				return rs.getInt(1);
			}else {
				return 0;
			}
		} catch (SQLException e) {
			return 0;
		}finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	//접속로그목록
	public ArrayList<SessionLog> selectLogs() {
		String sql=String.format("select * from test_counter order by c_id desc");
		ResultSet rs=Bitdb.executeQuery(sql);
		ArrayList<SessionLog> logs=new ArrayList<>();
		SessionLog log=null;
		try {
			while(rs.next()){
				log=new SessionLog();
				log.setC_id(rs.getInt("c_id"));
				log.setC_sessid(rs.getString("c_sessionid"));
				log.setC_ip(rs.getString("c_ip"));
				log.setC_sess(rs.getInt("c_sess"));
				log.setC_date(rs.getDate("c_date"));
				logs.add(log);
			}
			return logs;
		} catch (SQLException e) {
			return null;
		}finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
}

