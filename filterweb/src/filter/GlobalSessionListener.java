package filter;

import java.util.Objects;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class GlobalSessionListener implements HttpSessionListener {
	GlobalSessionDao dao;

	@Override
	public void sessionCreated(HttpSessionEvent e) {
		// 초기
		HttpSession session = e.getSession();
		dao = GlobalSessionDao.getInstance();
		ServletContext application = session.getServletContext();
		// 테이블에 로그기록
		SessionLog log = new SessionLog();
		log.setC_ip("");
		log.setC_sess(1);
		log.setC_sessid(session.getId());
		dao.insertLog(log);
		// 카운트 추가
		application.setAttribute("count", dao.getCount());
		application.setAttribute("logs", dao.selectLogs());
//		e.getSession().setAttribute("count", dao.getCount());
//		e.getSession().setAttribute("logs", dao.selectLogs());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent e) {
		// 초기
		HttpSession session = e.getSession();
		dao = GlobalSessionDao.getInstance();
		ServletContext application = session.getServletContext();
		// 테이블에 로그기록
		SessionLog log = new SessionLog();
		log.setC_ip("");
		log.setC_sess(0);
		log.setC_sessid(session.getId());
		dao.insertLog(log);
	}

}
