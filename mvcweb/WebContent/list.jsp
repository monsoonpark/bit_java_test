<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>list.jsp</title>
</head>
<body>
<h1>ADDRS LIST</h1>
<ul>
	<c:forEach items="${list}" var="i">
		<li>${i.AID}(${i.ZCODE}).${i.ADDR}.</li>
	</c:forEach>
</ul>
</body>
</html>