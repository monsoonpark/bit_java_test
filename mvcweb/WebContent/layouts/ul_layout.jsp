<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ul_layout.jsp</title>
<style type="text/css">
* {
	padding: 0px;
	margin: 0px;
}

/* li의 *없애기 */
li {
	list-style: none;
}
/* ul 위치잡기 */
#ulLayout {
	margin: 0 auto;
	width: 800px;
	border-left: thin solid gold;
	border-right: red thick solid;
}
/* 레이아웃 안쪽 여백 */
#ulLayout>li:nth-of-type(1), #ulLayout>li:nth-of-type(2), #ulLayout>li:nth-of-type(3)
	{
	padding: 10px;
}
/* TOP */
#ulLayout>li:nth-of-type(1) {
	border-bottom: thin dotted gray;
}
/* navigation */
#ulLayout>li:nth-of-type(1)>div {
	text-align: right;
}

#ulLayout>li:nth-of-type(1)  ul:nth-of-type(1) li {
	display: inline-block;
}
/* 링크의 밑줄 제거  */
#ulLayout>li:nth-of-type(1)  ul:nth-of-type(1) li a {
	text-decoration: none;
}

#ulLayout>li:nth-of-type(1)  ul:nth-of-type(1) li a:hover {
	color: green;
}

/* CONTENT */
#ulLayout>li:nth-of-type(2) {
	background-color: #eee;
}

/* 앨범테이블 */
.tbAlbum {
	width: 100%;
}

.tbAlbum img {
	width: 100%;
}

.tbAlbum td {
	padding: 5px;
}

.tbAlbum, .tbAlbum td {
	border: thin dotted gray;
	border-collapse: collapse;
}

/* BOTTOM */
#ulLayout>li:nth-of-type(3) {
	background-color: blue;
	color: gold;
}
</style>
<c:url var="tbcss" value="/css/table.css"></c:url>
<link href="${tbcss}" rel="stylesheet" type="text/css" />
</head>
<body>
	<ul id="ulLayout">
		<li><h1>BITWEB - TOP : 기본 세로 3단</h1>
			<div>
				<ul>
					<li>[<a href="#">MENU01</a>]
					</li>
					<li>[<a href="#">MENU02</a>]
					</li>
					<li>[<a href="#">MENU03</a>]
					</li>
					<li>[<a href="#">MENU04</a>]
					</li>
					<li>[<a href="#">MENU05</a>]
					</li>
				</ul>
			</div></li>
		<li>
			<ul>
				<!--  파일 업로드 폼 -->
				<li>
					<h1>FILE UPLOAD</h1> <c:url var="uploadpath" value="/"></c:url> <c:url
						var="upload" value="/uploadtest.do"></c:url>
					<form action="${upload}" method="post"
						enctype="multipart/form-data">
						<ul>
							<li>TITLE : <input type="text" name="f_title"
								placeholder="예>첨부파일이름" required="required" /></li>
							<li>FILE : <input type="file" name="f_file"
								required="required" /></li>
							<li>IMAGE FILE : <input type="file" name="f_image"
								required="required" /></li>
							<li><button>전송</button></li>
						</ul>
					</form>
				</li>
				<li>
					<h1>IMAGE ALBUM</h1>
					<table class="tbAlbum">
						<c:set var="i" value="1"></c:set>
						<c:forEach items="${list}" var="o">
							<c:if test="${i%3 == 1}">
								<tr>
							</c:if>
							<td><img alt="${o.f_title}"
								src="/mvcweb/images/${o.f_image}"></td>
							<c:if test="${i%3 == 0}">
								</tr>
							</c:if>
							<c:set var="i" value="${i=i+1}"></c:set>
						</c:forEach>
					</table>
				</li>
				<li>
					<h1>FILE LIBRARY</h1>
					<table class="tbgray">
						<thead>
							<tr>
								<th>NUM</th>
								<th>FILE NAME</th>
								<th>WRITER</th>
								<th>UPLOAD DATE</th>
								<th>IP</th>
								<th>첨부</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="o">
								<tr>
									<td>${o.fid}</td>
									<td>${o.f_title}</td>
									<td>AHN JUN</td>
									<td><fmt:formatDate value="${o.f_date}" var="fd"
											type="date" pattern="yyyy.MM.dd" />${fd}</td>
									<td>${o.f_ip}</td>
									<td>[<a href="download.do?f_file=${o.f_file}">${o.f_file}</a>]
									</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<!-- 페이징 그룹 이전 -->
									<c:if test="${pagenum > 10}">
										[<a href="uploadlist.do?pagenum=${startgrouppage-1}">PREV</a>]
									</c:if> <!-- 페이징 번호 -->
									<c:forEach
										begin="${startgrouppage}" end="${endgrouppage}" var="i"
										step="1">

										<!-- 현재페이지는 링크 없음 -->
										<c:if test="${i == pagenum}">${i}</c:if>
										<c:if test="${i != pagenum}">[<a
												href="uploadlist.do?pagenum=${i}">${i}</a>] 
   										</c:if>
									</c:forEach> 
									<!-- 페이징 그룹 다음 -->
									<c:if test="${endgrouppage < counts.allpages}">
										[<a href="uploadlist.do?pagenum=${endgrouppage+1}">NEXT</a>]
									</c:if></td>
							</tr>
						</tfoot>
					</table>
				</li>
			</ul>
		</li>
		<li>BOTTOM COPRYRIGHT &COPY; BITWEB</li>
	</ul>
</body>
</html>


