<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>customerlist.jsp</title>
<c:url var="tbcss" value="/css/table.css" />
<link href="${tbcss }" rel="stylesheet" type="text/css">
</head>
<body>
<h1>CUSTOMER LIST</h1>
<table class="tbgray">
	<thead>
		<tr>
			<td>고객번호</td>
			<td>이름</td>
			<td>주소</td>
			<td>전화번호</td>	
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list}" var="i"> 
			<tr>
				<td>${i.CUSTID}</td>
				<td class="testtd">${i.NAME}</td>
				<td>${i.ADDRESS }</td>
				<td>${i.PHONE}</td>
			<tr>
		</c:forEach>
	</tbody>
</table>
</body>
</html>