package mvc.uploads;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import mvc.command.CommandHandler;

public class ListHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Uploaddao dao = Uploaddao.getInstance();

		// select
		// 페이지번호
		String pagenum = req.getParameter("pagenum");
		if (Objects.isNull(pagenum) || pagenum.equals("")) {
			pagenum = "1";
		}
		req.setAttribute("list", dao.select(Integer.parseInt(pagenum)));
		req.setAttribute("counts", dao.selectCount());
		req.setAttribute("pagenum", pagenum);

		
		
		// 페이지그룹개수
		double grouppage = 10;
		req.setAttribute("grouppage", (int)grouppage);
		HashMap<String, Integer> map = (HashMap<String, Integer>)req.getAttribute("counts");
		int allpages = map.get("allpages");
		// 시작페이지번호
		int startgrouppage = 
		((int) Math.floor((Integer.parseInt(pagenum) - 1) / grouppage)) 
		* (int) grouppage + 1;
		req.setAttribute("startgrouppage", startgrouppage);

		// 종료페이지번호
		int endgrouppage = startgrouppage + ((int) grouppage - 1);
		endgrouppage = endgrouppage > allpages ? allpages : endgrouppage;
		req.setAttribute("endgrouppage", endgrouppage);

		return "/layouts/ul_layout.jsp";
	}

}
