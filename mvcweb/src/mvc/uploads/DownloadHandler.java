package mvc.uploads;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.command.CommandHandler;

public class DownloadHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setAttribute("f_file", req.getParameter("f_file"));
		req.setAttribute("filepath", req.getServletContext()
				.getRealPath("/images/"));
		return "/layouts/download.jsp";
	}

}
