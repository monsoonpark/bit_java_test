package mvc.uploads;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.command.CommandHandler;

public class Downloadtest implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String filename = "puppy.jpg";
		String filepath = req.getServletContext().getRealPath("/images/");
		req.setAttribute("filename", filename);
		req.setAttribute("filepath", filepath);
		return "/layouts/download.jsp";
	}

}
