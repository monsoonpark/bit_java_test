package mvc.uploads;

import java.util.Date;

public class Upload {
	// 멤버
	int fid;
	String f_title;
	String f_file;
	String f_image;
	Date f_date;
	String f_ip;
	// 생성자
	public Upload() {}
	public Upload(int fid, String f_title, String f_file, String f_image, Date f_date, String f_ip) {
		super();
		this.fid = fid;
		this.f_title = f_title;
		this.f_file = f_file;
		this.f_image = f_image;
		this.f_date = f_date;
		this.f_ip = f_ip;
	}
	//getter/setter
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public String getF_title() {
		return f_title;
	}
	public void setF_title(String f_title) {
		this.f_title = f_title;
	}
	public String getF_file() {
		return f_file;
	}
	public void setF_file(String f_file) {
		this.f_file = f_file;
	}
	public String getF_image() {
		return f_image;
	}
	public void setF_image(String f_image) {
		this.f_image = f_image;
	}
	public Date getF_date() {
		return f_date;
	}
	public void setF_date(Date f_date) {
		this.f_date = f_date;
	}
	public String getF_ip() {
		return f_ip;
	}
	public void setF_ip(String f_ip) {
		this.f_ip = f_ip;
	}
	
	
}
