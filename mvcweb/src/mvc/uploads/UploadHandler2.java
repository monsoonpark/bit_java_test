package mvc.uploads;

import java.io.File;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import mvc.command.CommandHandler;

public class UploadHandler2 implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// 초기
		MultipartRequest multi = null;
		Enumeration<String> filenames = null;
		String enctype = "utf-8";
		int uploadFileSizeLimit = 1024 * 1024 * 100; // 100메가
		String uploadpath = null;

		// 업로드 경로
		uploadpath = req.getServletContext().getRealPath("/images/");
		System.out.println("uploadpath : " + uploadpath);
		// 업로드 처리 객체 - 저장시점
		multi = new MultipartRequest(req, uploadpath, uploadFileSizeLimit, enctype
				, new DefaultFileRenamePolicy() {
			
			@Override
			public File rename(File f) {
				String filePath = f.getAbsolutePath().substring(0, f.getAbsolutePath().lastIndexOf("\\") + 1);
				String fileExtention = f.getName().substring(f.getName().lastIndexOf('.'));
				String result = null;
				for (int index = 0; index < 7; index++) // displays frequency
				{
					String newFileName = req.getParameter("fname") + Math.random();
					result = new StringBuffer(filePath).append(newFileName).append(fileExtention).toString();
					// return the new file
				}
				return new File(result);
			}
		});
		
		// 파일이름 가져오기
		filenames = multi.getFileNames();
		String inputname = null;
		while (filenames.hasMoreElements()) {
			inputname = filenames.nextElement();
			System.out.printf("inputname : %s \r\n", inputname);
			System.out.printf("전송한 파일이름 : %s \r\n", multi.getFilesystemName(inputname));
		}

		return "/layouts/ul_layout.jsp";
	}

}
