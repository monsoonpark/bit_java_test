package mvc.uploads;

import java.util.Enumeration;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import mvc.command.CommandHandler;

public class UploadHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// 초기
		MultipartRequest multi = null;
		Enumeration<String> filenames = null;
		String enctype = "utf-8";
		int uploadFileSizeLimit = 1024*1024*100; // 100메가
		String uploadpath = null;
		
		// 업로드 경로
		uploadpath = req.getServletContext().getRealPath("/images/");
		System.out.println("uploadpath : " + uploadpath);
		// 업로드 처리 객체 - 저장시점
		multi = new MultipartRequest(req, uploadpath, uploadFileSizeLimit
				, enctype, new DefaultFileRenamePolicy()); 
		// 파일이름 가져오기
		filenames = multi.getFileNames();
		String inputname = null;
		String[] fs = new String[2]; int i=0;
		while (filenames.hasMoreElements()) {
			inputname = filenames.nextElement();
			System.out.printf("inputname : %s \r\n"
			, inputname);
			fs[i++] = multi.getFilesystemName(inputname);
			System.out.printf("전송한 파일이름 : %s \r\n"
			, multi.getFilesystemName(inputname));
			
		}
		Uploaddao dao = Uploaddao.getInstance();
		
		// INSERT
		Upload up = new Upload(0,multi.getParameter("f_title")
				, fs[0], fs[1], null, req.getRemoteAddr());
		dao.insert(up);
		
		// select
			// 페이지번호
			String pagenum = req.getParameter("pagenum");
			if (Objects.isNull(pagenum) || pagenum.equals("")) {
				pagenum = "1";
			}
		req.setAttribute("list", dao.select(Integer.parseInt(pagenum)));
		req.setAttribute("counts", dao.selectCount());
		req.setAttribute("pagenum", pagenum);
		
		return "/layouts/ul_layout.jsp";
	}

}







