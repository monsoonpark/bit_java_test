package mvc.uploads;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import the.bits.Bitdb;

public class Uploaddao {
	// 멤버
	static Uploaddao dao;
	static {
		dao = new Uploaddao();
	}

	// 생성자
	private Uploaddao() {
	}

	// getInstance()
	public static Uploaddao getInstance() {
		return dao;
	}

	// INSERT
	public void insert(Upload up) {
		// 초기
		String sql = String.format(
				"INSERT INTO test_fup (fid, f_title, f_file, f_image, f_ip)\r\n"
						+ "VALUES (seq_test_fup.NEXTVAL, '%s', '%s', '%s', '%s')",
				up.getF_title(), up.getF_file(), up.getF_image(), up.getF_ip());
		// 쿼리실행
		Bitdb.execute(sql);
	}

	// SELECT
	public ArrayList<Upload> select() {
		// 초기
		String sql = "SELECT * FROM test_fup ORDER BY FID DESC";
		ArrayList<Upload> list = new ArrayList<>();
		Upload up = null;
		// 쿼리실행
		ResultSet rs = Bitdb.executeQuery(sql);
		try {
			while (rs.next()) {
				up = new Upload();
				up.setF_date(rs.getDate("f_date"));
				up.setF_file(rs.getString("f_file"));
				up.setF_image(rs.getString("f_image"));
				up.setF_ip(rs.getString("f_ip"));
				up.setF_title(rs.getString("f_title"));
				up.setFid(rs.getInt("fid"));
				//
				list.add(up);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 리턴
		return list;
	}

	// SELECT 페이지 별로
	public ArrayList<Upload> select(int pagenum) {
		// 초기
		String sql = String.format("SELECT * FROM V_test_fup WHERE FNC_TEST_PAGING(R,%s,10)=1", pagenum);
		ArrayList<Upload> list = new ArrayList<>();
		Upload up = null;
		// 쿼리실행
		ResultSet rs = Bitdb.executeQuery(sql);
		try {
			while (rs.next()) {
				up = new Upload();
				up.setF_date(rs.getDate("f_date"));
				up.setF_file(rs.getString("f_file"));
				up.setF_image(rs.getString("f_image"));
				up.setF_ip(rs.getString("f_ip"));
				up.setF_title(rs.getString("f_title"));
				up.setFid(rs.getInt("fid"));
				//
				list.add(up);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 리턴
		return list;
	}

	// 6. 전체 행의 개수 / 7. 전체 페이지의 개수
	public HashMap<String, Integer> selectCount() {
		// 초기
		String sql = "SELECT COUNT(*) allcounts, CEIL(COUNT(*)/10) allpages FROM test_fup";
		// 쿼리실행
		ResultSet rs = Bitdb.executeQuery(sql);
		// HashMap
		HashMap<String, Integer> map = null;
		try {
			if (rs.next()) {
				map = new HashMap<>();
				map.put("allcounts", rs.getInt("allcounts"));
				map.put("allpages", rs.getInt("allpages"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// 리턴
		return map;
	}
}
