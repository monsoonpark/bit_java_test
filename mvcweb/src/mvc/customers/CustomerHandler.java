package mvc.customers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.command.CommandHandler;

public class CustomerHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		//CustomerService.bindCust(req);
		req.setAttribute("list", CustomerDao.getInstance().selectList());
		return "/customers/customerlist.jsp";
	}

}
