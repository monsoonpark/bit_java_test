package mvc.customers;

import javax.servlet.http.HttpServletRequest;

public class CustomerService {
	// 멤버변수
	static CustomerDao dao;
	// static 블럭
	static {
		dao = CustomerDao.getInstance();
	}
	//
	/*
	public static void bindCust(HttpServletRequest r){
		r.setAttribute("customer", dao.selectList());
	}
	*/
}
