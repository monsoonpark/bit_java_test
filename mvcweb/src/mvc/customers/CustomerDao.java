package mvc.customers;

import java.util.ArrayList;
import java.util.HashMap;

import the.bits.Bitdb;

public class CustomerDao {
	// 멤버변수
	private static CustomerDao dao;
	// static 블력
	static {
		dao = new CustomerDao();
	}

	// 생성자
	private CustomerDao() {
	}

	// getInstance();
	public static CustomerDao getInstance() {
		return dao;
	}

	public ArrayList<HashMap<String, Object>> selectList() {
		String sql="SELECT * FROM customer ORDER BY custid DESC";
		ArrayList<HashMap<String, Object>> list=Bitdb.makeListMap(sql);

		return list;
	}

}
