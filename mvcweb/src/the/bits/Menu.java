package the.bits;

import java.io.Serializable;

public class Menu implements Serializable {
	// 멤버변수
	String name; // 메뉴이름
	int num; // 메뉴번호
	
	// 생성자
	public Menu(){}
	public Menu(String name, int num) {
		super();
		this.name = name;
		this.num = num;
	}
	
	// getter / setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
	
}
