package the.bits;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Bitawt {
	// 프레임을 전달받아 창닫기 이벤트 추가
	public static void bindClose(Frame f){
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
	}
}
