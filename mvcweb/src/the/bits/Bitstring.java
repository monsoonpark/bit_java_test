package the.bits;

import java.util.Objects;
import java.util.Scanner;

public class Bitstring {
	// �������
	static String num = "0123456789";
	static String alpha = "abcdefghijklmnopqrstuvwxyz";
	static Scanner s = new Scanner(System.in);
	
	// String�� input�ϸ� ���������
	public static void printSubtitle(String pLabel){
		System.out.printf("== %s == \r\n", pLabel);
	}
	
	// �������� �Է¹޾� String���� ����
	public static String inputMultiString(String pLabel){
		// �ʱ�
		StringBuffer sb = new StringBuffer();
		String temp = null;
		// ��
		System.out.printf("%s (�Է¿Ϸ�: ���ٿ� x �Է��� ����) :", pLabel);
		do {
			temp = s.nextLine();
			if (temp.equals("x")) {
				return sb.toString();
			}
			sb.append(temp);
			sb.append("\r\n");
		} while (true); 
	}
	
	
	// �Է¹޾Ƽ� int�� ����
	public static int inputInt(String pLabel){
		// �ʱ�
		int input = 0;
		System.out.printf("%s :", pLabel);
		do {
			try {
				input = Integer.parseInt(s.nextLine());
				return input;
			} catch (Exception e) {
				System.out.printf("�ٽ��Է����ּ���!(%s) \r\n"
						, e.getMessage());
			} 
		} while (true);
	}
	
	
	// ���ڿ� �Է¹޾Ƽ� String�� ����
	public static String inputString(String pLabel){
		// �ʱ�
		String input = null;
		System.out.printf("%s :", pLabel);
		do {
			try {
				input = s.nextLine();
				return input;
			} catch (Exception e) {
				System.out.printf("�ٽ��Է����ּ���!(%s) \r\n"
						, e.getMessage());
			} 
		} while (true);
	} 
	
	
	// Ư�����ڿ��� �޾Ƽ� �ѱ��̸� true�� ����
	public static Boolean isHangle(String pStr){
		// �ʱ� �� ~ ��
		char chr = 0;
		
		if (Objects.isNull(pStr) || pStr.equals("")) {
			return false;
		}
		for (int i=0; i<pStr.length(); i++) {
			chr = pStr.charAt(i);
			// �ѱ� ���ڰ� �ƴϸ� false ����
			if (chr < 'ㄱ' || chr > '힝') {
				return false;
			}
		}
		// ����
		return true;
	}

	// ���ڿ��� �޾Ƽ� ���� ���ĺ��� ȥ�뿩�ο� ���Ǵ� ���ĺ����� Ȯ���ؼ�
	// ������ true
	public static Boolean isNumAlpha(String pStr) {
		// �ʱ�
		Boolean usedNum = false;
		Boolean usedAlpha = false;

		// ����
		// ���� ���ĺ��� ������ ���ڰ� ������ ����
		for (int i = 0; i < pStr.length(); i++) { 
			if (!(num.indexOf(pStr.charAt(i)) >= 0 
			|| alpha.indexOf(pStr.toLowerCase().charAt(i)) >= 0)) {
				return false;
			}
		}
		// ���� �ִ��� Ȯ��
		for (int i = 0; i < pStr.length(); i++) {
			if (num.indexOf(pStr.charAt(i)) >= 0) {
				usedNum = true; break;
			}
		};	
	   // ���ĺ��� �ִ��� Ȯ��
		for (int i = 0; i < pStr.length(); i++) {
			if (alpha.indexOf(pStr.toLowerCase().charAt(i)) >= 0) {
				usedAlpha = true; break;
			}
		}

		// ��ȯ
		return usedNum && usedAlpha;

	}

	// ����(�ּҰ����� �ִ밳��)�� �޾Ƽ� �����ǰ����� �����ȿ� ������ true
	// ������ false
	public static Boolean isRange(String pStr, int pStart, int pEnd) {
		// �ʱ�
		int len = pStr.length();
		return len >= pStart && len <= pEnd;
	}

	// ��ü���ڰ����� �޾Ƽ� ������ �ʰ��ϸ� ... ���̱�
	public static String cutString(String pStr, int pLen) {
		// ��� ...�����ؼ� pLen�� �����ϰ�
		if (pStr.length() > pLen && pLen > 3) {
			return pStr.substring(0, pLen - 3).concat("...");
		}
		return pStr;
	}

	// ��Ʈ���� ���޹޾� ���ĺ����� �ƴ��� ����(���ĺ��̸� true)
	public static Boolean isAlpha(String pStr) {
		// ������ ���ڰ� ���ĺ��� �ƴϸ� false�� ����
		for (int i = 0; i < pStr.length(); i++) {
			if (alpha.indexOf(pStr.toLowerCase().charAt(i)) < 0) {
				return false;
			}
		}
		// ��ȯ
		return true;
	}

	// ��Ʈ���� ���޹޾� ������ �ƴ��� ����(���� true)
	public static Boolean isNumber(String pStr) {
		// ������ ���ڰ� ���� �ƴϸ� false�� ����
		for (int i = 0; i < pStr.length(); i++) {
			if (num.indexOf(pStr.charAt(i)) < 0) {
				return false;
			}
		}
		// ��ȯ
		return true;
	}
}
