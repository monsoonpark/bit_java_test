package bit.services;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

public class GoService {
	// 멤버변수
	static Godao dao;
	// static 블럭
	static {
		dao = Godao.getInstance();
	}
	// modelA
	public static void bindAddrs(HttpServletRequest r){
		r.setAttribute("list", dao.selectList());
	}
}
