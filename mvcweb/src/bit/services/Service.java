package bit.services;

import java.util.ArrayList;

public class Service {
	// modelA
	public ArrayList<String> returnModelA(){
		ArrayList<String> al = new ArrayList<>();
		al.add("수지");
		al.add("아이린");
		al.add("손예진");
		
		return al;
	}
	// modelB
	public ArrayList<String> returnModelB(){
		ArrayList<String> al = new ArrayList<>();
		al.add("곰돌이푸, 행복한 일은 매일 있어");
		al.add("나,있는 그대로 참 좋다");
		al.add("모든 순간이 너였다");
		
		return al;
	}

}
