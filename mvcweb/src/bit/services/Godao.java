package bit.services;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import the.bits.Bitdb;

public class Godao {
	// 멤버변수
	private static Godao dao;
	// static 블력
	static {
		dao = new Godao();
	}

	// 생성자
	private Godao() {
	}

	// getInstance();
	public static Godao getInstance() {
		return dao;
	}

	// select addrs
	public ArrayList<HashMap<String, Object>> selectList() {
		// 초기
		ArrayList<HashMap<String, Object>> list = new ArrayList<>();
		HashMap<String, Object> hm = null;
		// select
		ResultSet rs = Bitdb.executeQuery("SELECT * FROM ADDRS ORDER BY AID DESC");
		// rs => ArrayList<HashMap<String, String>>
		try {
			ResultSetMetaData md = rs.getMetaData();
			while (rs.next()) {
				hm = new HashMap<>();
				for (int i = 1; i <= md.getColumnCount(); i++) {
					hm.put(md.getColumnName(i), rs.getString(i));
				}
				list.add(hm);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return list;
	}

}
