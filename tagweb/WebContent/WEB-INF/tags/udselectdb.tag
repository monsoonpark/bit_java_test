<%@tag import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@tag import="the.test.Code"%>
<%@tag import="java.util.ArrayList"%>
<%@tag import="java.sql.ResultSet"%>
<%@tag import="the.bits.Bitdb"%>
<%@ tag body-content="empty" language="java" pageEncoding="UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@ attribute name="tablename" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="listname" required="true" rtexprvalue="false" type="java.lang.String" %>
<%@ variable name-from-attribute="listname" alias="codelist" variable-class="java.lang.String" scope="AT_END" %>
<%
	// select
	ResultSet rs = Bitdb.executeQuery("SELECT * FROM " + tablename);
	// ResultSet -> ArrayList<Code>
	ArrayList<Code> codes = new ArrayList<>();
	while (rs.next()) {
		codes.add(new Code(rs.getString(2), rs.getInt(1)));
	}
	// setAttribute
	request.setAttribute("codes", codes);
%>

<c:set var="codelist" >
	<c:forEach items="${codes}" var="c">
		<ul>
			<li>${c.name}</li>
		</ul>
	</c:forEach>
</c:set>
