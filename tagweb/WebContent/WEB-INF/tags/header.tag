<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="title" required="true" %>
<%@ attribute name="level" type="java.lang.Integer" %>

<%
	String st=null;
	String en=null;
	if(level==null){
		st="<h1>";
		en="</h1>";
	}else{
		st=String.format("<h%s>", level);
		en=String.format("</h%s>", level);
	}
%>

<%=st%>${title}<%=en%>