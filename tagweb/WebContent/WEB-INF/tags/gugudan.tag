<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="sdan" required="true" type="java.lang.Integer" %>
<%@ attribute name="edan" required="true" type="java.lang.Integer" %>
<%@ variable name-given="result" variable-class="java.lang.String" scope="NESTED" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="result" value="${''}"></c:set>
<c:forEach var="dan" begin="${sdan }" end="${edan }">
	<c:set var="result" value="${result=result.concat('<h3>').concat(dan).concat('단:').concat('</h3>') }"></c:set>
	<c:forEach var="num" begin="${1 }" end="${9 }">
		<c:set var="result" 
		value="${result=result.concat('<h1>'.concat(dan).concat('x').concat(num).concat('=').concat(dan*num).concat('</h1>'))}"></c:set>	
	</c:forEach>
</c:forEach>
<jsp:doBody/>	