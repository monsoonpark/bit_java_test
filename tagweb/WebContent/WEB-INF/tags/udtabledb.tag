<%@tag import="java.sql.ResultSetMetaData"%>
<%@ tag body-content="empty" language="java" pageEncoding="UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@tag import="java.sql.ResultSet"%>
<%@ tag import="the.bits.Bitdb"%>
<%@ attribute name="tablename" required="true"%>
<%@ attribute name="table" type="java.lang.String" rtexprvalue="false"
	required="true"%>
<%@ variable name-from-attribute="table" alias="result"
	variable-class="java.lang.String" scope="AT_END"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	// select
	ResultSet rs = Bitdb.executeQuery("SELECT * FROM " + tablename);
	// ResultSet -> ArrayList<Code>
	// 태그 만들기
	StringBuffer sb = new StringBuffer();
	sb.append("<table border='1'>");
	// 1. 헤더 만들기
	ResultSetMetaData rsmd = rs.getMetaData();
	sb.append("<tr>");
	for (int i = 1; i <= rsmd.getColumnCount(); i++) {
		sb.append(String.format("<th>%s</th>", rsmd.getColumnName(i)));
	}
	sb.append("</tr>");
	// 2. 데이터 로우 만들기
	while(rs.next()){
		sb.append("<tr>");
		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			sb.append(String.format("<td>%s</td>",rs.getString(i)));
		}
		sb.append("</tr>");
	}
	
	sb.append("</table>");
%>
<c:set var="result" value="<%=sb.toString()%>"></c:set>

