<%@tag import="the.test.Code"%>
<%@tag import="java.util.ArrayList"%>
<%@tag import="java.sql.ResultSet"%>
<%@tag import="the.bits.Bitdb"%>
<%@ tag body-content="scriptless" language="java" pageEncoding="UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@ attribute name="type" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:doBody var="bodyTest" scope="page"/>
<c:set var="splits" value="$ {fn:split(bodyTest,',') }"/>
<!-- type이 li인 경우 -->
<c:if test="${type eq 'li'}">
	<ul>
	<c:forEach items="${splits }" var="sp">
		<li>${sp }</li>
	</c:forEach>
	</ul>
</c:if>

<c:if test="${type eq 'table' }">
	<table>
		<c:forEach items="${splits }" var="sp">
			<tr>
				<td >${sp }</td>
			</tr>
		</c:forEach>
	</table>
</c:if>

<c:if test="${type eq 'dbtable' }">

	<c:forEach items="${splits }" var="sp">
		<c:set var="sp" value="${sp }" scope="request" />
<%
	//select
	ResultSet rs = Bitdb.executeQuery("SELECT * FROM " +request.getAttribute("sp"));
	// ResultSet -> ArrayList<Code>
	ArrayList<Code> codes = new ArrayList<>();
	while (rs.next()) {
		codes.add(new Code(rs.getString(2), rs.getInt(1)));
	}
	// setAttribute
	request.setAttribute("codes", codes);
%>
		<table>
		 	<c:forEach items="${codes}" var="c">
		 		<tr><td>${c }</td></tr>
		 	</c:forEach>
		</table>
	</c:forEach>
</c:if>
