<%@tag import="the.test.Code"%>
<%@tag import="java.util.ArrayList"%>
<%@tag import="java.sql.ResultSet"%>
<%@tag import="the.bits.Bitdb"%>
<%@ tag body-content="scriptless" language="java" pageEncoding="UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@ attribute name="type" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:doBody var="content" scope="page"/>
<%
	String content=(String)jspContext.getAttribute("content");

	if(type.equals("li")){
		String[] arrs=content.split(",");		
		request.setAttribute("arrs", arrs);
%>
	<ul>
	<c:forEach items="${arrs }" var="arr">
		<li>${arr }</li>
	</c:forEach>
	</ul>
<%
	}else if(type.equals("table")){
		String[] arrs=content.split(",");		
		request.setAttribute("arrs", arrs);	
%>
	<table style="border:1px black solid">
	<c:forEach items="${arrs }" var="arr">
		<tr>
			<td style="border:1px black solid">${arr }</td>
		</tr>
	</c:forEach>
	</table>
<%
	}else if(type.equals("dbtable")){
		String[] arrs=content.split(",");		
		request.setAttribute("arrs", arrs);	
%>
	<c:forEach items="${arrs }" var="arr">
	<c:set var="ar" scope="request">${arr }</c:set>
<%
	//select
	ResultSet rs = Bitdb.executeQuery("SELECT * FROM " +request.getAttribute("ar"));
	// ResultSet -> ArrayList<Code>
	ArrayList<Code> codes = new ArrayList<>();
	while (rs.next()) {
		codes.add(new Code(rs.getString(2), rs.getInt(1)));
	}
	// setAttribute
	request.setAttribute("codes", codes);
%>
		<table style="border:1px black solid">
		<c:forEach items="${codes }" var="a">
			<tr>
				<td style="border:1px black solid">${a }</td>
			</tr>
		</c:forEach>
		</table>
	</c:forEach>
<%
	}
%>