<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tf" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
	table, tr, td{
		border-collapse: collapse;
		border: 1px black solid;
	}
</style>
<body>
	<h1>수지 등등을 li태그로 반환</h1>
	<tf:lists type="li">수지,아이린,아이유</tf:lists>
	<h1>수지 등등을 table태그로 반환</h1>
	<tf:lists type="table">공유,로다주,크리스햄스워스</tf:lists>
	
	<h1>태그안의 테이블 이름에 해당하는 내용을 table태그로 반환</h1>
	<tf:lists type="dbtable">SPHONE_COMCODE,CODESMARTPHONE</tf:lists>
	
	<hr>
	<tf:lists2 type="li">수지,아이린,아이유</tf:lists2>
	<hr>
	<tf:lists2 type="table">공유,로다주,크리스햄스워스</tf:lists2>
	<hr>
	<tf:lists2 type="dbtable">SPHONE_COMCODE,CODESMARTPHONE</tf:lists2>
</body>
</html>