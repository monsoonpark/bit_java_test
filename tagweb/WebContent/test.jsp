<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tf" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script>
	function initMap() {
		var mapPos = {
			lat : 37.499481,
			lng : 127.029330
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom : 18,
			center : mapPos
		});
		var marker = new google.maps.Marker({
			position : mapPos,
			map : map
		});
		
	}
</script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr1WbmiuFZ-W5lRxTbZSMuN8TpZdU3mUs&callback=initMap" async defer>
</script>
<style>
	#map {
		width: 100%;
		height: 400px;
		background-color: whitegrey;
	}
</style>
<link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css">
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>
<body>
	<tf:udselectdb tablename="SPHONE_COMCODE" listname="listA"/>
	${listA }
	<hr/>
	<tf:udselectdb tablename="CODESMARTPHONE" listname="listB"/>
	${listB }
	<hr/>
	<tf:udtabledb tablename="SPHONE_COMCODE" table="liA" />
	${liA }
	<hr/>
	<tf:udtabledb tablename="guestbook_message" table="liB" />
	${liB }
	
	
	
	<button onclick="addMarker()">등록</button>
	
<h3>My Google Maps Demo</h3>
<div id="map"></div>
</body>
</html>