<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="tf" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="dateEL" value="<%=new Date() %>"/>	
	<tf:removeHtmlVar var="removed" trim="true">
		<font size="10"> 현재<style>시간</style>은 ${dateEL }입니다.</font>
	</tf:removeHtmlVar>
	
	처리결과:${removed }
</body>
</html>