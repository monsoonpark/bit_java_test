package the.test;

public class Code {
	//멤버변수
	String name;
	int code;
	
	//생성자
	public Code(){}
	public Code(String name, int code) {
		super();
		this.name = name;
		this.code = code;
	}
	
	// getter/ setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return String.format("%s. %s",code,name );
	}
	
	
}
