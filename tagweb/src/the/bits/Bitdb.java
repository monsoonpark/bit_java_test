package the.bits;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Objects;

public class Bitdb {

	
	//멤버변수//초기
	static Connection conn=null;
	static Statement stmt=null;
	
	
	//초기화블럭
	static{
		try {
			//1.드라이버 로딩
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
		} catch (Exception e) {
			System.out.printf("에외: %s ",e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	//db연결
	public static void open() throws Exception{

		conn=DriverManager.getConnection("jdbc:apache:commons:dbcp:tagweb");
		if(Objects.isNull(conn) || conn.isClosed()){
			throw new Exception("DB연결 실패");
		}

	}
	public static void close() throws SQLException{
		if(!Objects.isNull(conn) && !conn.isClosed()){
			conn.close();
		}
	}
	//SELECT처리 메서드
	public static ResultSet executeQuery(String sql){
		//초기
		ResultSet rs=null;
		
		try {
			//연결
			open();
			//쿼리
			stmt=conn.createStatement();
			//리턴
			return stmt.executeQuery(sql);
		} catch (Exception e1) {
			System.out.printf("Bitdb executeQuery() 예외 : %s",e1.getMessage());
			e1.printStackTrace();
		}
		//리턴
		return null;
		
	}
	
	//DML(INSERT, DELETE, UPDATE)처리 메서드
	public static void execute(String sql){
		//오라클접속
		try {
			//연결
			open();
			//sql문장 실행
			stmt=conn.createStatement();
			stmt.execute(sql);
			
		}catch (Exception e1) {
			System.out.printf("Bitdb execute() 예외 : %s",e1.getMessage());
			e1.printStackTrace();
		}finally{
			try {
				close();
			} catch (SQLException e) {
				System.out.printf("Bitdb execute() 예외 : %s",e.getMessage());
				
			}
		}
	}
}
