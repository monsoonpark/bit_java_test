<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="top.jspf"></jsp:include>
<!-- CONTENTS  -->
<li style="background-color: yellow;">
	<ul>
		<li><h1>WHILE 구구단 출력</h1>
			<ul>
				<%
					int i=1;
					
					while(i<=9) {
				%>
				<li>
					<h1><%=i%>단
					</h1>
					<ul>
						<%
							int j=1;
							while(j<=9){
						%>
						<li><%=i%> X <%=j%> = <%=i * j%></li>
						<%
							j++;
							}
						%>
					</ul>
				</li>
				<%
					i++;
					}
				%>
			</ul></li>
	</ul>
</li>
<jsp:include page="bottom.jsp"></jsp:include>