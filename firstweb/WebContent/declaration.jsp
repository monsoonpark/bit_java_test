<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="top.jspf"></jsp:include>
<%!
	//멤버코딩
	public static int initnum=7;
	
	public int getNum(){
		return 7*1;
	}
%>

<!-- CONTENTS  -->
<li style="background-color: yellow;">
	<ul>
		<li>
			<h1>DECLARATION 예제</h1>
			<ul>
				<li><%=this.getNum() %></li>
				<li><%=initnum %></li>
			</ul>
		</li>
	</ul>
</li>
<jsp:include page="bottom.jsp"></jsp:include>