<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="top.jspf"></jsp:include>
<!-- CONTENTS  -->
<li style="background-color: yellow;">
	<ul>
		<li><h1>FOR 구구단 출력</h1>
			<ul>
				<%
					for (int i = 1; i <= 9; i++) {
				%>
				<li>
					<h1><%=i%>단
					</h1>
					<ul>
						<%
							for (int j = 1; j <= 9; j++) {
						%>
						<li><%=i%> X <%=j%> = <%=i * j%></li>
						<%
							}
						%>
					</ul>
				</li>
				<%
					}
				%>
			</ul></li>
	</ul>
</li>
<jsp:include page="bottom.jsp"></jsp:include>