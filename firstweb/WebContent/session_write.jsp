<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>APPLICATION_TEST</title>
</head>
<body>
<%request.setCharacterEncoding("utf-8"); %>
	<%
		String sname=request.getParameter("sname");
		sname=Objects.isNull(sname)? session.getAttribute("sname").toString():sname;

		session.setAttribute("sname", sname);
	%>	
	<h1>${sname }</h1>
	<form action="session_write.jsp" method="post">
		<ul>
			<li><input type="text" name="sname" required="required" placeholder="이름" /></li>
			<li><button name="login" value="로그인"></button></li>
		</ul>
	</form>
</body>
</html>