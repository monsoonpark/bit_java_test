<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<jsp:include page="top.jspf"></jsp:include>

<!-- CONTENTS  -->
<li style="background-color: yellow;">
	<ul>
		<ul>
			<li>
				<%
					Map<String,String[]> map=request.getParameterMap();
					out.println(String.format("map.size() : %s",map.size()));
					
				%>
			</li>
			<li>
				<%
					Enumeration<String> names=request.getParameterNames();	//map에 대한 key값
					String name, value;
					while(names.hasMoreElements()){
						name=names.nextElement();
						value=request.getParameter(name);
						out.println(String.format("name : %s, value : %s <hr/>",name,value ));
					}
				%>
			</li>
		</ul>
		<li>
			<h1>GETPARAMETER 예제</h1>
			<ul>
				<li><a href="getparameter.jsp">매개변수 없이 링크</li>
				<li><a href="getparameter.jsp?name=hongkildong">매개변수 1개</li>
				<li><a href="getparameter.jsp?name=hongkildong&age=27">매개변수 2개</li>
				<li>
					<form action="getparameter.jsp" method="post">
						이름: <input name="name" placeholder="ex)홍길동" required="required"/><br/>
						나이: <input name="age" placeholder="ex)27" required="required"/><br/>
						직업: <select name="job">
								<option value="1">자바개발자</option>
								<option value="2">닷넷개발자</option>
								<option value="3">빅데이터개발자</option>
								
								</select><br/>
						<button>전송</button>
					</form>	
				</li>
			</ul>
		</li>
	</ul>
</li>
<jsp:include page="bottom.jsp"></jsp:include>