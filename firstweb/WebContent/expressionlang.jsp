<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
    <!-- trimDirectiveWhitespaces="true" 위에 공백 제거 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>expressionlang.jsp</title>
</head>
<body>
<% 
	//request의 setAttribute
	request.setAttribute("KEY", "VALUE");	//key, value
	request.setAttribute("name", "홍길동");
	request.setAttribute("age", "27");
	
%>
	<h1>Expression Language</h1>
	<ul>
		<li><%=request.getAttribute("KEY") %></li>
		<li><%=request.getAttribute("name") %></li>
		<li><%=request.getAttribute("age") %></li>
		<li>${KEY }</li>
		<li>${name }</li>
		<li>${age }</li>
		<li><%=request.getParameter("name")%></li>
		<li><%=request.getParameter("age")%></li>
		<li>${param.name }</li>
		<li>${param.age }</li>	
		
	</ul>
</body>
</html>