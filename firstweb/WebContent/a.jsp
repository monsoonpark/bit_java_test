<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>a.jsp
</title>
</head>
<body>
	<%
		//INSERT 처리
		String para=request.getParameter("state");
		para=Objects.isNull("para")? "no":para;
		if(para.equals("yes")){
			//페이지 이동
			response.sendRedirect("yes.jsp");
		}else{
			response.sendRedirect("no.jsp");
		}
	%>
</body>
</html>