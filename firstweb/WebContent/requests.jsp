<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
    <!-- trimDirectiveWhitespaces="true" 위에 공백 제거 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>requests.jsp</title>
</head>
<body>
	<h1>REQUEST 객체</h1>
	<ul>
		<li>사용자 아이피: <%=request.getRemoteAddr()%></li>
	</ul>
	
	<h1>CHECKBOX 처리</h1>
	<form action="requests.jsp" type="post">
		<ul>
			<li>취미 : 
				<input type="checkbox" name="hobby" value="1"/>게임
				<input type="checkbox" name="hobby" value="2"/>운동
				<input type="checkbox" name="hobby" value="3"/>야구관람
			</li>
			<li>
				<button>전송</button>	
			</li>
			
		</ul>
	</form>
	<ul>
		<!-- 전송하면 체크한 값만큼 li로 출력 -->
		<%
			String[] paras=request.getParameterValues("hobby");
			String[] hobby={"게임","운동","야구관람"};
			if(!Objects.isNull(paras)){
				for(String v:paras){
					out.println("<li>"+hobby[Integer.parseInt(v)-1]+"</li>");
				}
			}else{
				out.println("<li>전송데이터 없음</li>");
			}
		%>
	</ul>
	
</body>
</html>