<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>APPLICATION_TEST</title>
</head>
<body>
<%request.setCharacterEncoding("utf-8"); %>
	<%
		String name=request.getParameter("name");
		name=Objects.isNull(name)? application.getAttribute("name").toString():name;
		//application에 name키 추가
		application.setAttribute("name", name);
	%>	
	<h1>${name }</h1>
	<form action="application_write.jsp" method="post">
		<ul>
			<li><input type="text" name="name" required="required" placeholder="이름" /></li>
			<li><button>전송</button></li>
		</ul>
	</form>
</body>
</html>