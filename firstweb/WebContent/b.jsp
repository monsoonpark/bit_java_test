<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>b.jsp</title>
</head>
<body>
	<h1>${param.state }</h1>
	<%
		String state=request.getParameter("state");
		System.out.println("state : "+state);
		//response.sendRedirect("bb.jsp?state="+state);
		response.sendRedirect("bb.jsp?state="+URLEncoder.encode(state));
	%>
</body>
</html>