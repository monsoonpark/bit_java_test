<%@page import="java.util.Objects"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="the.bits.Bitdb"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<jsp:include page="top.jspf"></jsp:include>

<!-- CONTENTS  -->
<%
	//Bitdb.execute(sql);
	
	String title, id;
	title=request.getParameter("otitle");
	id=request.getParameter("oid");
	if(!(Objects.isNull(title) || title.equals(""))){
		String sql=String.format("insert into wlist(num,title,id)values (%s,'%s','%s')", "seq_wlist.nextval",title,id );
		Bitdb.execute(sql);
	}
	//out.println(title+id);
%>
<%
	String sql="select num, title, id from wlist order by num";
	ResultSet rs=Bitdb.executeQuery(sql);
%>
<li style="background-color: yellow;">
	<ul>
		<li>
			<h1>Bitdbtest 예제</h1>
		</li>
		<!-- 입력폼 -->
		<li>
			<form action="bitdbtest.jsp" method="post">
				<input type="hidden" name="state" value="insert"/>
				제목: <input type="text" name="otitle" placeholder="ex)First reply" required="required"/><br/>
				아이디: <input type="text" name="oid" placeholder="ex)ID" required="required"/><br/>
				<button>전송</button>
			</form>
		</li>
		<!-- 목록 -->
		<li>
			<h1>한줄방명록 목록</h1>
			<ul>
				<%while(rs.next()){ %>
					<li> 
					<%=String.format("%s.%s - %s", rs.getInt("num"),rs.getString("title"),rs.getString("id")) %>
					</li>
				<%}%>
			</ul>
		</li>
	</ul>
</li>
<jsp:include page="bottom.jsp"></jsp:include>