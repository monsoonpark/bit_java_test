package the.bits;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Objects;

//파일 입출력을 처리해주는 공통클래스
public class Bitfile {
	//타입을 유지한채 쓰기
	public static void writeObject(Object pObj, String pFilename){
		//초기
//		Bitfile.readObjects(pFilename);
		String path=String.format("c:\\javaws\\firstproj\\%s",pFilename);
		
		try {
			FileOutputStream fos=new FileOutputStream(path, false);
			BufferedOutputStream bos=new BufferedOutputStream(fos);
			ObjectOutputStream out=new ObjectOutputStream(bos);	
			out.writeObject(pObj);
			out.close();
			
			//메세지
			System.out.println("내용이 등록되었습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}	//파일생성
		
	}
	
	//파일타입을 유지한채 읽어오기
	public static Object readObjects(String pFilename){
		String path=String.format("c:\\javaws\\firstproj\\%s",pFilename);
		
		try {
			FileInputStream fis=new FileInputStream(path);
			BufferedInputStream bis=new BufferedInputStream(fis);
			ObjectInputStream in=new ObjectInputStream(bis);
			//메세지
			System.out.println("내용을 읽었습니다.");
			//반환
			return in.readObject();
			
		} catch (Exception e) {
			System.out.printf("%s 파일은 없습니다.\r\n",pFilename);
//			e.printStackTrace();
		}	//파일생성
		return null;
		
	}
	
	//파일내용 읽어오기
	public static ArrayList<String> readStrings(String pFilename){
		BufferedReader br=null;	//파일에 기록한 내용을 메모리상에서 처리..
		String strrow=null;		//기록내용
		ArrayList<String> strings=new ArrayList<String>();
		
		//읽어오기
		try {
			br=new BufferedReader(new FileReader(new File(String.format("c:\\javaws\\firstproj\\%s",pFilename))));
			//한줄씩 읽어오면서 출력
			while(!Objects.isNull(strrow=br.readLine())){
				strings.add(strrow);
			}
			
			//메세지
			System.out.printf("%s파일의 내용을 전부 읽었습니다. \r\n",pFilename);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return strings;
		
	}
	
	
	
	//파일에 텍스트 쓰기
	public static void writeString(String pRow, String pFilename){
		//초기화
		FileWriter fw=null;		//실제 파일의 관리
		BufferedWriter bw=null;	//파일에 기록할 내용을 메모리상에서 처리..
//		String strrow=pRow;		//기록내용
		File f=null;			//파일객체
		
		//기록
		f=new File(String.format("C:\\javaws\\firstproj\\%s",pFilename));
		try {
			fw=new FileWriter(f, true);	// FileWriter(파일객체, 텍스트의 누적여부) append
			bw=new BufferedWriter(fw);	
			bw.write(String.format("%s \r\n",pRow));
			bw.close();					//실제 파일이 저장되는 시점
			//메세지
			System.out.println("파일이 기록되었습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}
	//
	

}
