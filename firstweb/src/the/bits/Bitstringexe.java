package the.bits;

import java.util.Scanner;

public class Bitstringexe {

	public static void main(String[] args) {
		//아이디를 입력, Scanner /do while 이용, 규칙 숫자 알파뱃을 혼용 6~10자 일치하면 아이디 등록
		Scanner s=new Scanner(System.in);
		
		String id=null;
		
		do{
			System.out.println("등록할 ID를 입력하세요(영문,숫자혼용 6~10자리)");
			id=s.nextLine();
//			if(!Bitstring.isRange(6, 10, id)==true){
//				System.out.println("범위안의 문자수를 입력하세요(6~10)");
//				continue;
//			}
//			if(!Bitstring.isNumAlpha(id)==true){
//				System.out.println("영문과 숫자로만 입력가능합니다.");
//				continue;
//			}
//			break;
//		}while(true);
		}while(!Bitstring.isRange(6, 10, id) || !Bitstring.isNumAlpha(id));

		System.out.println("ID가 등록되었습니다.");
		
		s.close();
	}

}
