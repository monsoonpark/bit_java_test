package the.bits;

import java.util.Objects;
import java.util.Scanner;

public class Bitstring {
	//멤버변수
	static String num="0123456789";
	static String alpha="abcdefghijklmnopqrstuvwxyz";
	static Scanner s=new Scanner(System.in);;
	
	//String을 input하면 소제목 출력
	public static String printSubtitle(String pLabel){
		System.out.printf("== %s ==\r\n",pLabel);
		return null;
	}
	
	//여러줄 입력받아 String으로 리턴
	public static String inputMultiString(String pLabel){
		//초기
		StringBuffer sb=new StringBuffer();
		String temp=null;
		//라벳
		System.out.printf("%s (한줄에 x입력후 엔터):",pLabel);
		do{
			temp=s.nextLine();
			if(temp.equals("x")){
				return sb.toString();
			}
			sb.append(temp);
			sb.append("\r\n");
		}while(true);
		
	}
	
	//입력을 받아서 int로 리턴
	public static int intputInt(String pLabel){
		int input=0;
		System.out.printf("%s :",pLabel);
		do{
			try {
				input=Integer.parseInt(s.nextLine());
				return input;
			} catch (Exception e) {
				System.out.printf("다시입력해주세요(%s) \r\n",e.getMessage());
			}
		}while(true);
	}
	
	//문자열 입력받기
	public static String inputString (String pLabel){
		//초기
		String input=null;
		System.out.printf("%s :",pLabel);
		do{
			try {
				input=s.nextLine();
				return input;
			} catch (Exception e) {
				System.out.printf("다시입력해주세요(%s) \r\n",e.getMessage());
			}
		}while(true);
	}

	
	//특정문자열을 받아서 한글이면 true를 리턴
	public static Boolean isHangle(String pStr){
		//초기 ㄱ~힝
		char chr=0;
				
		if(Objects.isNull(pStr) || pStr.equals("")){
				return false;
		}
		
		for (int i=0; i<pStr.length();i++) {
			chr=pStr.charAt(i);
			//한글문자가 아니면 false리턴
			if(chr<'ㄱ' || chr>'힣'){
				return false;
			}
		}
		return true;	//리턴
	}
	
	//문자열받아서 숫자, 알파뱃 혼용 여부와 수 또는 알파뱃인지 확인 맞으면 true
	public static Boolean isNumAlpha(String pStr){
		Boolean usedNum=false;
		Boolean usedAlpha=false;
		//검증
		//수와 알파벳을 제외한 문자가 있으면 중지
		for(int i=0; i<pStr.length(); i++){
			if(!(num.indexOf(pStr.charAt(i))>=0 || alpha.indexOf(pStr.toLowerCase().charAt(i))>=0)){
				return false;
			}
		}
		
		//각각의 문자가 수
		for(int i=0; i<pStr.length(); i++){
			//수를 사용했는지 확인
			if(num.indexOf(pStr.charAt(i))>=0){
				usedNum=true;
				break;
			}
		}
		// 또는 알파벳
		for(int i=0; i<pStr.length(); i++){
			//알파벳을 사용했는지 확인
			if(alpha.indexOf(pStr.toLowerCase().charAt(i))>=0){
				usedAlpha=true;
				break;
			}
		}
		if(pStr.equals("")){
			return false;
		}
		//반환
		return usedNum && usedAlpha;
	}
	
	//범위(최소, 최대 수)를 받아서, 문자의 개수가 범위안이면 true else false
	public static boolean isRange(int pStart, int pEnd, String pStr){
		int len=pStr.length();
//		if(len<pStart || len>pEnd){
//			return false;
//		}
		return len>= pStart && len <=pEnd;
	}
	
	//전체문자 갯수를 받아서 수 초과시 ...붙이기
	public static String cutString(String pStr, int pLen){
		if(pStr.length()>pLen && pLen>3){
			pStr=pStr.substring(0, pLen-3).concat("...");
		}
		return pStr;
	}
	
	//String을 전달 받아 알파벳인지 아닌지 리턴(알파벳이면 true)
	public static boolean isAlpha(String pStr){
		//각각의 문자가 알파벳이 아니면 false를 리턴
		for(int i=0; i<pStr.length(); i++){
			if(alpha.indexOf(pStr.toLowerCase().charAt(i))<0){
				return false;
			}
		}
		if(pStr==""){
			return false;
		}
		//반환
		return true;
	}
	
	//String 전달 숫자인지 아닌지 리턴(true, false)
	public static boolean isNumber(String pStr){
		//각각의 문자가 수가 아니면 false를 리턴
		for(int i=0; i<pStr.length(); i++){
			if(num.indexOf(pStr.charAt(i))<0){
				return false;
			}
		}
		if(pStr==""){
			return false;
		}
		//반환
		return true;
	}


}

