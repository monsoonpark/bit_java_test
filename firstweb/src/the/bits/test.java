package the.bits;

import java.util.Scanner;

public class test {
	 public static void main(String[] args) {
		  /*
		   * * 아이디를 입력 Scanner / do while 사용
		   * 규칙 수,알파벳을 혼용해서 6~10자
		   * 일치하면 아이디가 등록되었습니다.
		   */
		  // 초기
		  Scanner s = new Scanner(System.in);
		  String input = "";
		  
		  do {
		   // 입력
		   System.out.print("아이디(알파벳을 혼용해서 6~10자) : ");
		   input = s.nextLine();
		   
		  } while (!Bitstring.isRange(6, 10, input) 
		    || !Bitstring.isNumAlpha(input));

		  System.out.println("아이디가 등록되었습니다.");
		 }
}
