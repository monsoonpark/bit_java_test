package the.bits;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Bitawt {
	public static void bindClose(Frame f){
		//윈도우 창닫기
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);	
			}
		});
	}
	public static void bindClose(Dialog d){
		//윈도우 창닫기
		d.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);	
			}
		});
	}
}
