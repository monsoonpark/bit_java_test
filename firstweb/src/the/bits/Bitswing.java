package the.bits;

import java.awt.Desktop;
import java.awt.peer.DesktopPeer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

//스윙 관련 공통클래스
public class Bitswing {
	//멤버변수 

	//쿼리를 전달받아 콤보박스에 바인딩
	public static void bindCombo(String sql, JComboBox<Code> cbx){
		//초기
		Code c=null;
		ResultSet rs=Bitdb.executeQuery(sql);
		//코드추가
		try {
			while(rs.next()){
				cbx.addItem(new Code(rs.getString(2), rs.getInt(1)));
			}
		} catch (SQLException e) {
			System.out.printf("bindCombo 예외: %s",e.getMessage());
		}finally{
			try{
				Bitdb.close();
			}catch(SQLException e){
			}
		}
	}
	
	//internalframe 2개를 받아 한개는 dispose
	//한개는 visible
	public static void changeFrame(JDesktopPane dp, JInternalFrame oldjf, JInternalFrame newjf){
		//이전 것을 dispose
		if(!Objects.isNull(oldjf)){
			oldjf.dispose();
		}
		//새 것을 visible
		((javax.swing.plaf.basic.BasicInternalFrameUI)newjf.getUI()).setNorthPane(null);
		newjf.setBounds(0, 0, 785, 600);
		newjf.setBorder(null);
		newjf.setVisible(true);
		dp.add(newjf);
	}
}
