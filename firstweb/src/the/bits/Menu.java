package the.bits;

import java.io.Serializable;

//메뉴처리를 위한 공통클래스
public class Menu implements Serializable{
	//Lunch Menu Selection Console Application
	//메뉴관리
	String name;	//메뉴이름
	int num;		//메뉴번호
	
	//생성자
	public Menu(String name, int num) {
		super();
		this.name = name;
		this.num = num;
	}
	public Menu() {
		super();
	}
	
	//getter / setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
	



}
