package the.bits;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Bitmenu {
	
	//멤버변수
	ArrayList<Menu> menus;
	int selectedIndex;
	Scanner s;
	String filename;	//메뉴가 저장될 경로;
	String title;	//메뉴의 프로그램 이름
	
	//생성자
	public Bitmenu() {
		this.filename="bitmenu.dat";
		this.s=new Scanner(System.in);
		this.menus=(ArrayList<Menu>)Bitfile.readObjects(this.filename);
		this.menus=Objects.isNull(this.menus)? new ArrayList<>():this.menus;
	}
	public Bitmenu(String filename) {
		this.filename=filename;
		this.s=new Scanner(System.in);
		this.menus=(ArrayList<Menu>)Bitfile.readObjects(this.filename);
		this.menus=Objects.isNull(this.menus)? new ArrayList<>():this.menus;
	}
	public Bitmenu(String filename, ArrayList<Menu> menus) {
		this.filename=filename;
		this.s=new Scanner(System.in);
		this.menus=menus;
		Bitfile.writeObject(this.menus, this.filename);
	}
	public Bitmenu(String filename, ArrayList<Menu> menus, String title) {
		this.title=title;
		this.filename=filename;
		this.s=new Scanner(System.in);
		this.menus=menus;
		Bitfile.writeObject(this.menus, this.filename);
	}
	//메서드
	//프로그램 시작
	public void printAppstart(){
		System.out.println("===============================================");
		System.out.printf("========= %20s START! ========= \r\n",this.title);
		System.out.println("===============================================");
			
	}
	//프로그램 종료
	public void printAppend(){
		System.out.println("===============================================");
		System.out.printf("========= %20s E N D! ========= \r\n",this.title);
		System.out.println("===============================================");	
	}
	
	//getter/ setter
	//메뉴선택
	public void getMenus() {
		//초기
		System.out.println("\r== 메뉴 선택 ==");
		for (Menu menu : menus) {
			System.out.printf("%2s.%s \r\n", menu.getNum(),menu.getName());
		}
		
		//선택
		try{
			System.out.print("선택: ");
			String temp=s.nextLine();
			temp=Bitstring.isNumber(temp)? temp:"0";
			this.selectedIndex=Integer.parseInt(temp);
		}catch(Exception e){
			System.out.println("잘못된 입력으로 0번이 선택됩니다.");
			this.selectedIndex=0;
		}
		
	}
	public void setMenus() {
		//초기
		Menu menu;
		//메뉴등록
		System.out.println("--메뉴등록 시작");
		do{
			System.out.println("메뉴이름:(Enter는 종료)");
			menu=new Menu();
			menu.setName(s.nextLine());
			if(menu.name.equals("")){
				break;
			}
			System.out.print("메뉴번호: ");
			menu.setNum(Integer.parseInt(s.nextLine()));
			//munus에 menu추가
			menus.add(menu);
			
		}while(true);
		
		//메뉴를 저장
		Bitfile.writeObject(this.menus, this.filename);
		System.out.println("--메뉴등록 끝");
	}
	public int getSelectedIndex() {
		return selectedIndex;
	}
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
}