package the.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import the.bits.Bitdb;

/**
 * Servlet implementation class Forward_select
 */
@WebServlet("/forward_select.do")
public class Forward_select extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Forward_select() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ResultSet rs=Bitdb.executeQuery("select * from test_cnations order by ncode desc" );
		StringBuffer select=new StringBuffer();
		
		select.append("<select name='cnation'>");
		try {
			while(rs.next()) {
				select.append("<option value='");
				select.append(rs.getString("ncode"));
				select.append("'>");
				select.append(rs.getString("nname"));
				select.append("</option>");
			}
		} catch (SQLException e) {
		}
		select.append("</select>");
		request.setAttribute("select", select.toString());
		request.getRequestDispatcher("forward_select.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
