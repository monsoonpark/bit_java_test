package the.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import the.bits.Bitdb;

/**
 * Servlet implementation class Select_gbook
 */
@WebServlet("/select_gbook.do")
public class Select_gbook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Select_gbook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("utf-8");
		ResultSet rs=Bitdb.executeQuery("select gid,gtitle,gcontent,gdate,gip from jsp_gbook order by gid desc");
		StringBuffer sb=new StringBuffer();
		sb.append("<ul>");
		try {
			while(rs.next()) {
				sb.append("<li>");
				sb.append(rs.getString("gid")+". ");
				sb.append(rs.getString("gtitle")+"<br>");
				sb.append(rs.getString("gcontent")+"<br>");
				sb.append(rs.getString("gdate")+" ip: ");
				sb.append(rs.getString("gip"));
				sb.append("</li><br>");
			}
		} catch (SQLException e) {
		}
		sb.append("</ul>");
		request.setAttribute("select", sb);
		request.getRequestDispatcher("select_gbook.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
