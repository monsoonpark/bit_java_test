package the.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import the.bits.Bitdb;

/**
 * Servlet implementation class Insert_gbook
 */
@WebServlet("/insert_gbook.do")
public class Insert_gbook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insert_gbook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//한글 입력값 
		request.setCharacterEncoding("utf-8");
		//초기
		String gtitle=request.getParameter("title");
		String gcontent=request.getParameter("content");
		String gip=request.getRemoteAddr();
		
		//입력가능 여부 판단
		if(Objects.isNull(gtitle) || Objects.isNull(gcontent)) {
//		if(Objects.isNull(gtitle) ) {
			response.sendRedirect("er-ror.jsp");
		}else {
			//등록처리
//			if(gcontent.equals(null)||gcontent.equals("")) {
//				gcontent=" ";
//			}
			String sql=String.format("insert into jsp_gbook(gid,gtitle,gcontent,gip) values(%s,'%s','%s','%s')",
					"seq_jsp_gbook.nextval",
					gtitle,
					gcontent,
					gip);
			Bitdb.execute(sql);
			
		}
		//setAttribute
		request.setAttribute("title", gtitle);
		request.setAttribute("ip", gip);
		
		//forward
		request.getRequestDispatcher("insert_gbook_ok.jsp").forward(request, response);
		//등록완료 페이지 insert_gbook_ok.jsp
		
		
	}
	

}
