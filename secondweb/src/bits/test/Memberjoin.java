package bits.test;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Memberjoin
 */
@WebServlet("/memberjoin.act")
public class Memberjoin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Memberjoin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		Jstl_member jm=new Jstl_member();
		jm.setMuserid(request.getParameter("muserid"));
		jm.setMpwd(request.getParameter("mpwd"));
		jm.setMname(request.getParameter("mname"));
		jm.setMip(request.getRemoteAddr());
		
		//insert
//		if(Objects.isNull((jm=Jstl_member_dao.checkLogin(jm)))) {
//			Jstl_member_dao.insertMember(jm);
//		}
		Jstl_member_dao.insertMember(jm);
		//forward
		request.getRequestDispatcher("memberjoinok.jsp").forward(request, response);
		//memberjoinok.jsp

	}

}
