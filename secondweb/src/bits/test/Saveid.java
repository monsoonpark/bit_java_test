package bits.test;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Saveid
 */
@WebServlet("/saveid.act")
public class Saveid extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Saveid() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//아이디저장 체크 확인
		String value=request.getParameter("cbIdsave");
		String userid=request.getParameter("userid");
		System.out.println(value);
		
		if(!Objects.isNull(value) && value.equals("yes") && !Objects.isNull(userid) && !userid.equals("")) {
			//아이디 저장
			response.addCookie(new Cookie("userid", userid));
		}
		
		//saveid.jsp로 이동
		response.sendRedirect("saveid.jsp");
	}

}
