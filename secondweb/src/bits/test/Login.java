package bits.test;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login.act")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//파라미터
		
		//초기
		request.setCharacterEncoding("utf-8");
		Jstl_member m=new Jstl_member();
		m.setMuserid(request.getParameter("muserid"));
		m.setMpwd(request.getParameter("mpwd"));
		//null인지 확인
		if(Objects.isNull(m.getMuserid()) || m.getMuserid().equals("") || Objects.isNull(m.getMpwd()) || m.getMpwd().equals("")) {
			
		}else {
			//아이디 비밀번호 정상입력
			if((!Objects.isNull(m=Jstl_member_dao.checkLogin(m)))) {
				//로그인
				HttpSession session=request.getSession();
				session.setAttribute("m", m);
				session.setAttribute("login", "ok");
			}
			
		}
		
		//index로 redirect
		response.sendRedirect(request.getContextPath()+"/index.jsp");
		
	}

}
