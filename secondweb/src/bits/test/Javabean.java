package bits.test;

public class Javabean {
	//멤버변수
	String name_j;
	int age;
	
	//생성자
	public Javabean() {
		super();
	}

	public Javabean(String name_j, int age) {
		super();
		this.name_j = name_j;
		this.age = age;
	}

	//getter/setter
	public String getName_j() {
		return name_j;
	}

	public void setName_j(String name_j) {
		this.name_j = name_j;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
}
