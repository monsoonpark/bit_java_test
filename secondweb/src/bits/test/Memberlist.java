package bits.test;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Memberlist
 */
@WebServlet("/memberList.do")
public class Memberlist extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Memberlist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ArrayList<Jstl_member> list=Jstl_member_dao.listMember();
		//HttpSession session=request.getSession();
		//session.setAttribute("list", list);
		request.setAttribute("list", Jstl_member_dao.listMember());
		(request.getRequestDispatcher("memberlist.jsp")).forward(request, response);
		
		//response.sendRedirect(request.getContextPath()+"/memberlist.jsp");
	}

	

}
