package bits.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import the.bits.Bitdb;

public class Jstl_member_dao {
	//회원가입
	public static void insertMember(Jstl_member m) {
		String sql=String.format("insert into jstl_member(mid,mname,muserid,mpwd,mip) values(seq_jstl_member.nextval,'%s','%s','%s','%s')",
				m.getMname(),
				m.getMuserid(),
				m.getMpwd(),
				m.getMip()
				);
		Bitdb.execute(sql);
		System.out.println("Jstl_member_dao insertMember 실행완료");
	};
	
	//회원목록
	public static ArrayList<Jstl_member> listMember(){
		ArrayList<Jstl_member> ms=new ArrayList<>();
		Jstl_member jm=null;
		String sql="select * from jstl_member order by mid desc";
		ResultSet rs=Bitdb.executeQuery(sql);
		try {
			while(rs.next()) {
				jm=new Jstl_member();
				jm.setMid(rs.getInt("mid"));
				jm.setMip(rs.getString("mip"));
				jm.setMname(rs.getString("mname"));
				jm.setMuserid(rs.getString("muserid"));
				ms.add(jm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ms;
	};
	
	//로그인체크
	public static Jstl_member checkLogin(Jstl_member m) {
		String sql=String.format("select * from jstl_member where muserid='%s' and mpwd='%s'",
				m.getMuserid(),m.getMpwd());
		
		ResultSet rs=Bitdb.executeQuery(sql);
		try {
			if(rs.next()) {
				//로그인
				m.setMname(rs.getString("mname"));
				return m;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	
	
}