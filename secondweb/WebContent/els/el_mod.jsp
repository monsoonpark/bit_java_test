<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
	/*CSS 클래스 정의*/
	.bg_gold {
		background-color: gold;
		font-size: 25px;	
	}
	.bg_silver{
		background-color: silver;
		font-size: 30px;
	}
</style>
</head>
<body>
	<h1>EL MOD</h1>
	<p>Jstl과 el 나눈 나머지로 총 10개행 배경색이 번갈아 출력</p>
	<ul>
		<c:forEach begin="1" end="10" step="1" var="i">
			<li class="${i%2==0 ? 'bg_gold':'bg_silver'}"/>${i }</li>
			
		</c:forEach>

		
		
	</ul>
</body>
</html>