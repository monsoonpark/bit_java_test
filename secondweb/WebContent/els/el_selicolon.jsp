<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>SEMICOLON</h1>
	<c:set var="c" value="6"></c:set>
	<ul>
		<li>${c=c+1; c }</li>
		<li>${c; c+=1 }</li>
		
	</ul>
	
</body>
</html>