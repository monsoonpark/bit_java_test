<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	request.setAttribute("name", "공유");
	session.setAttribute("name", "수지");
	application.setAttribute("name", "아이린");
	pageContext.setAttribute("name", "페이지");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>EL TEST	</h1>
	<ul>
		<li>request의 name= <input type="text" value="${requestScope.name }"/></li>
		<li>page의 name= <input type="text" value="${pageScope.name }"/></li>
		<li>session의 name= <input type="text" value="${sessionScope.name }"/></li>
		<li>application의 name= <input type="text" value="${applicationScope.name }"/></li>
		<li>기본의 name= <input type="text" value="${name }"/></li>
		<!-- page, request, session, application순으로 우선 -->	
		
	</ul>
</body>
</html>