<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>EL COLLECTION</h1>
	<h2>LIST</h2>
	<p>리눅스마스터 1급, 네트워크관리사 1급, 정보처리기사</p>
	<br /> c:set으로 list 생성후 li에 출력
	<c:set var="cert_li" value="리눅스마스터 1급, 네트워크관리사 1급, 정보처리기사"></c:set>
	<ul>
		<c:forEach items="${cert_li }" var="c">
			<li>${c }</li>
		</c:forEach>
	</ul>

	<h2>MAP</h2>
	<p>
		certA.리눅스마스터 1급, certB.네트워크관리사 1급 , certC.정보처리기사 <br />
		c:set 으로 map생성후 li에 출력
		<c:set var="cert_map"
			value="${{ 'certA': { 'name' : '리눅스마스터 1급'}, 
						'certB': { 'name' : '네트워크관리사'}, 
						'certC': { 'name' : '정보처리기사'} }}"></c:set>
	</p>
	<ul>
		<c:forEach items="${cert_map}" var="m">
			<li>${m.key}: ${m.value.name}</li>
		</c:forEach>
	</ul>

	<h2>SET</h2>
	<p>리눅스마스터 1급, 네트워크관리사 1급, 정보처리기사</p>
	<br /> c:set으로 set 생성후 li에 출력
	<c:set var="cert_set"
		value="${ {'리눅스마스터 1급','네트워크관리사 1급','정보처리기사','정보처리기사'} }">
	</c:set>
	<ul>
		<c:forEach items="${cert_set }" var="s">
			<li>${s }</li>
		</c:forEach>
	</ul>
</body>
</html>