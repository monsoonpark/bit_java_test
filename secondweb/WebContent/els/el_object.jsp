<%@page import="java.util.ArrayList"%>
<%@page import="bits.test.Jstl_member"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>el_object</title>
</head>
<body>
	<%
		HashMap<String, Jstl_member> ms=new HashMap<>();
		ms.put("m1", new Jstl_member(1,"홍길동","hong","1234","128.0.0.1"));
		ms.put("m2", new Jstl_member(2,"이길동","lee","1234","128.0.0.2"));
		ms.put("m3", new Jstl_member(3,"오길동","oh","1234","128.0.0.3"));
		//hashmap을 arraylist로 변환
		ArrayList<Jstl_member> maps=new ArrayList<>(ms.values());
		//page의 setAttribute
		pageContext.setAttribute("maps", maps);
		pageContext.setAttribute("ms", ms);
		
		ArrayList<Jstl_member> as= new ArrayList<>();
		as.add( new Jstl_member(1,"홍길동","hong","1234","128.0.0.1"));
		as.add( new Jstl_member(2,"이길동","lee","1234","128.0.0.2"));
		as.add( new Jstl_member(3,"오길동","oh","1234","128.0.0.3"));
		pageContext.setAttribute("as", as);
		
	%>
	<h1>HASHMAP LIST</h1>
	<ul>
		<c:forEach items="${maps }" var="m">
			<li>${m.mid }. ${m.mname }(${m.muserid }, ${m.mip })</li>
		</c:forEach>
	</ul>
	<ul>
		<c:forEach items="${ms }" var="m">
			<li>${m.value.mid }. ${m.value.mname }(${m.value.muserid }, ${m.value.mip })</li>
		</c:forEach>
	</ul>
	
	<h1>ArrayList LIST</h1>
	<ul>
		<c:forEach items="${as }" var="a">
			<li>${a.mid }. ${a.mname }(${a.muserid }, ${a.mip })</li>
		</c:forEach>
	</ul>
</body>
</html>