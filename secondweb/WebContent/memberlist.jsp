<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="top.jsp"></jsp:include>
<style>
	table,tr,td{
		border: 1px black solid;
		border-collapse: collapse;
		padding:5px;
		text-align: center;
	}
</style>
	<%
		
	%>
	<!-- CONTENT -->
	<li id="lic">
		<h1 style="background-color: #00ffff; text-align: center;">MEMBER LIST</h1>
		<table>
			<thead>
			<tr>
				<td>회원번호</td>
				<td>회원아이디</td>
				<td>회원이름</td>
				<td>ip</td>
			</tr>
			</thead>
			<tbody>
			<c:forEach items="${list}" var="l">
				<tr>
					<td>${l.mid }</td>
					<td>${l.muserid }</td>
					<td>${l.mname }</td>
					<td>${l.mip}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		
	</li>
		
<jsp:include page="bottom.jsp"></jsp:include>