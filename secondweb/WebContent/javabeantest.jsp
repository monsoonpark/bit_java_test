<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>javabeantest.sjp</title>
</head>
<body>
	<h1>JAVA BEAN TEST</h1>
	<form action="javabean_test_action.jsp" method="post">
		<ul>
			<li>이름 : <input type="text" name="name_j" required="required" placeholder="eeluem"/></li>
			<li>나이 : <input type="text" name="age" required="required" placeholder="99"/></li>
			<li><button>전송</button></li>
		</ul>
	</form>
</body>
</html>