<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<jsp:include page="top.jsp"></jsp:include>

<style>
	table,td,th{
		border: thin tan solid;
		border-collapse: collapse; 
	}
	th,td{
		padding:5px;
	}
	thead{
		background-color: #ddffdd;
		text-align: center;
	}
	.tr_bg td:nth-of-type(1) {
		background-color: #dd00dd;
	}
	
	
</style>
	
	<!-- CONTENT -->
	<li id="lic">
		<h1 style="text-align: center; background-color: #ffff55;">영화목록</h1>
		<table>
			<thead >
				<tr>
					<td>번호</td>
					<td>분류</td>
					<td>영화제목</td>
				</tr>
			</thead>
			<tbody>
				<tr class="tr_bg">
					<td rowspan="2">1</td>
					<td rowspan="2">SF</td>
					<td>어벤져스</td>
				</tr>
				<tr>
					<td>램페이지</td>
				</tr>
				<tr class="tr_bg">
					<td rowspan="2">2</td>
					<td rowspan="2">드라마</td>
					<td>바람바람바람</td>
				</tr>
				<tr>
					<td>덕구</td>
				</tr>
			</tbody>
		</table>
	</li>
	<li>
		${a }
	</li>
<jsp:include page="bottom.jsp"></jsp:include>