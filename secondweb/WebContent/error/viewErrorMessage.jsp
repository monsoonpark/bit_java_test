<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<jsp:include page="../top.jsp"></jsp:include>

	<!-- CONTENT -->
	<li id="lic">
		<b>예외 발생</b>
		<ul>
			
			<li>에러 타입: <%=exception.getClass().getName() %></li>
			<li>에러 메세지: <%=exception.getMessage() %></li>
			
		</ul>
	</li>
		
<jsp:include page="../bottom.jsp"></jsp:include>