<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<jsp:include page="top.jsp"></jsp:include>
<%
	Cookie[] cs=request.getCookies();
	if(!Objects.isNull(cs) && cs.length>0){
		for(Cookie c: cs){
			if(c.getName().equals("userid")){
				request.setAttribute("userid_c", c.getValue());
			}
		}
	}
%>
	<!-- CONTENT -->
	<li id="lic">
		<h1>아이디 저장하기</h1>
		<form action="saveid.act" method="post">
			<ul>
				<li>아이디: <input type="text" name="userid" value="${userid_c }" required="required" placeholder="ID!"/></li>
				<li>비밀번호: <input type="password" name="pwd" required="required" placeholder="PW!"/></li>
				<li><input type="checkbox" name="cbIdsave" value="yes"/>아이디저장</li>
				<li><button>전송</button></li>
			</ul>
		</form>
	</li>
	
<jsp:include page="bottom.jsp"></jsp:include>