<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		/*
		String id=application.getInitParameter("user");
		String pw=application.getInitParameter("pwd");
		if(request.getParameter("id").equals(id) && request.getParameter("pw").equals(pw)){
			session.setAttribute("login", "ok");
			session.setAttribute("lname", "홍길동");
			response.sendRedirect("login.jsp");
		}else{
			response.sendRedirect("logout.jsp");
		}
		*/
		//초기
		String user=request.getParameter("id");
		String pwd=request.getParameter("pw");
		//판단
		if(Objects.isNull(user) || Objects.isNull(pwd) || 
			!user.equals(application.getInitParameter("user")) ||
			!pwd.equals(application.getInitParameter("pwd"))){
			//로그인불가
		
		}else{
			//로그인
			session.setAttribute("login", "ok");
			session.setAttribute("lname", user);
		}
		response.sendRedirect("login.jsp");
	%>
	
</body>
</html>