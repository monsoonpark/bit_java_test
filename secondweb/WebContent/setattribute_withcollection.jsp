<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="top.jsp"></jsp:include>
<style>
	table, tr, td{
		border:1px black solid; 
		border-collapse: collapse;
	}
</style>
<!-- CONTENT -->
<li id="lic">
	<%
		//ArrayList
		ArrayList<String> arrs=new ArrayList<>();
		arrs.add("우타카밀 핸드크림");
		arrs.add("로즈 핸드크림");
		arrs.add("체리블라썸 소프트 핸드크림");
		
		//arrs를 다음과 같이 table태그에 출력(jstl사용)
		// 우타카밀
		// 로즈
		// 체리
		request.setAttribute("arrs", arrs);
		
		//순위, 제조사, 이름
		/*
			1,록시땅, 시어버터 핸드크림
			2,카밀, 핸드 앤 네일 크림 클래식
			3,세타필, 모이스춰라이징 크림
		*/
		ArrayList<HashMap<String, Object>> arrs2=new ArrayList<>();
		HashMap hm=null;
		
		hm=new HashMap();
		hm.put("rankey", 1);
		hm.put("company", "록시땅");
		hm.put("title", "시어버터 핸드크림");
		arrs2.add(hm);
		
		hm=new HashMap();
		hm.put("rankey", 2);
		hm.put("company", "카밀");
		hm.put("title", "핸드 앤 네일 크림 클래식");
		arrs2.add(hm);
		
		hm=new HashMap();
		hm.put("rankey", 3);
		hm.put("company", "세타필");
		hm.put("title", "모이스춰라이징 크림");
		arrs2.add(hm);
		
		request.setAttribute("arrs2", arrs2);
	%>
		<ul>
			<c:forEach var="arr2" items="${arrs2}">
				<li><input type="radio" name="hc" value="${arr2.rankey }"/>${arr2.rankey }. ${arr2.title }(${arr2.company })</li>
			</c:forEach>
		</ul>
		<hr color="redishblue" width="90%" align="center">
		<table>
			<c:forEach var="arr" items="${arrs }">
				<tr>
					<td>${arr }</td>
				</tr>
			</c:forEach>
		</table>
		<hr color="redishblue" width="90%" align="center">
		<select style="height: 30px;">
			<c:forEach var="arr" items="${arrs }">
				<option value="${arr }">${arr }</option>
			</c:forEach>
		</select>
		
</li>
		
<jsp:include page="bottom.jsp"></jsp:include>