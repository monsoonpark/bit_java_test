<%@page import="java.util.Objects"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>popup.jsp</title>
<script text="text/javascript">
	function isPopup(pFrm){	
		//alert(pFrm.popup.checked);
		//체크판단
		if(pFrm.popup.checked){
			//다시안열기 처리
			return true;
		}else{
			window.close();	//현재 창 닫기
			return false;
		}
	}
	
</script>
</head>
<body>
	
	<h1>POPUP</h1>
	<ul>
		<li>popup text</li>
		<li>
			<form action="popupclose.jsp" method="get" onsubmit="return isPopup(this);" >
				<input type="checkbox" name="popup" value="no"/>다시 열지 않기<br/>
				<button>닫기</button>
			</form>
		</li>
	</ul>
</body>
</html>