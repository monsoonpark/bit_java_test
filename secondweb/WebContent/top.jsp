<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<ul>
		<!-- TOP -->
		<li id="lit"><h1>WEBSITR LOGO</h1>
			<ul>
				<!-- 로그인 안한경우 -->
				<c:if test="${empty login || login !='ok'}">
					<li>
						<form action="login.act" method="post">
							USER ID: <input type="text" name="muserid" required="required" placeholder="ex) hong"/>
							USER PW: <input type="password" name="mpwd" required="required" placeholder="ex) 1234"/>
							<button>LOGIN</button><br>	
							<a href="memberjoin.jsp">SIGN IN</a>
						</form>
					</li>
				</c:if>
				<!-- 로그인 한경우 -->
				<c:if test="${!empty login && login == 'ok'}">
					<li>
						${m.mname }(${m.muserid }) 로그인중
						<a href="logout.act">LOGOUT</a>
					</li>
				</c:if>
				
			</ul>
		</li>
		<!-- NAVIGATION -->
		<li id="lin">
			<ul>
				<li><a href="memberList.do">MEMBER LIST</a></li>
				<li><a href="readParameter.jsp">ERROR PAGE</a></li>
				<li><a href="table_element.jsp">TABLE TAGA</a></li>
				<li><a href="setattribute_withcollection.jsp">setAttribute</a></li>
				<li><a href="javabeantest.jsp">JAVA BEAN</a></li>
				<li><a href="saveid.jsp">아이디기억하기</a></li>
				<li><a href="nopopup">팝업</a></li>
				
			</ul>
		</li>
		