<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("utf-8"); %>
<jsp:useBean id="jb" class="bits.test.Javabean"></jsp:useBean>
<jsp:setProperty property="*" name="jb"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>javabean_test.action.jsp</title>
</head>
<body>
	<h1>받은 데이터</h1>
	<ul>
		
		<li>이름: ${jb.name_j }</li>
		<li>나이: ${jb.age }</li>
		<li>이름: <jsp:getProperty property="name_j" name="jb"/></li>
		<li>나이: <jsp:getProperty property="age" name="jb"/></li>
	</ul>
</body>
</html>