<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 람다식: 함수를 식처럼 -->
	<!-- 람다식>식으로 함수를 구현> 정의:  -->
	${ greaterThan=(a,b) -> a > b ? true : false; '' }
	<!-- (a,b) 선언부,매개변수 정의 //// -> 선언부와 몸체 구분자 -->
	<!-- a>b?true:false 리턴값 -->
	<!-- 람다식 사용 -->
	<h1>${ greaterThan(7,77) }</h1>
	<h1>${ greaterThan(100,77) }</h1>
	
	<hr/>
	<!-- 정의 -->
	${factorial =(n) -> n==1 ? 1 : n*factorial(n-1); '' }	
	<h1>${factorial(5) }</h1>
</body>
</html>