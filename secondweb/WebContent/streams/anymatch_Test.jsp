<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	${list1= [1,4,8,10,15]; list2= [2,3,6 ]; list3= [1,3,5]; map= {
	'l1' :list1, 'l2': list2, 'l3':list3 } ; '' }
	<h1>anyMatch(), allMatch(), noneMatch()</h1>
	<p>
		map의 요소 3개의 리스트에 대해서
		다음과 같이 출력
	</p>
	<h2>map에서 3을 포함하는 리스트를 li에 출력</h2>
	<ul>
		<c:forEach items="${map}" var="m">
			<c:if test="${m.value.stream().anyMatch(v -> v eq 3).get() }">
				<li>
					<h1>${m }</h1>
					<ul>
						<c:forEach items="${m.value }" var="i">
							<li>${i }</li>
						</c:forEach>
					</ul>
				</li>
			</c:if>
		</c:forEach>	
	</ul>
	
	<h2>map에서 모두 홀수인 리스트를 li에 출력</h2>
	<ul>
		<c:forEach items="${map}" var="m">
			<c:if test="${m.value.stream().allMatch(v -> v%2==1).get() }">
				<li>
					<h1>${m }</h1>
					<ul>
						<c:forEach items="${m.value }" var="i">
							<li>${i }</li>
						</c:forEach>
					</ul>
				</li>
			</c:if>
		</c:forEach>
	</ul>
	
</body>
</html>