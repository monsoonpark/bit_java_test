<%@page import="java.util.ArrayList"%>
<%@page import="bits.test.Javabean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	ArrayList<Javabean> jbs=new ArrayList<>();
	jbs.add(new Javabean("Que sera sera",3) );
	jbs.add(new Javabean("사필귀정",7) );
	jbs.add(new Javabean("Everythings gonna be fine",12) );
	jbs.add(new Javabean("Good Luck",15) );
	jbs.add(new Javabean("인생사 새옹지마",8) );
	jbs.add(new Javabean("Be Glad",15) );
	
	pageContext.setAttribute("list", jbs);
%>

	<h1>STREAM 기본 메서드</h1>
	<p>age가 10이하인 Javabean은 제외</br>
		나머지의 합을 구해서 h1태그에 출력
	</p>
		
	<h1>${list.stream().filter(x -> x.age >= 10).map(x -> x.age).sum() }</h1>
	<h1>${list.stream().filter(x -> x.age >= 10).map(x -> x.age).average().get() }</h1>
	<c:forEach items="${list.stream().filter(x -> x.age >= 10).map(x -> x.name_j).toList() }" var="l">
		<p>${l }</p>
	</c:forEach>	
	
	<h1>SORTED</h1>
	<p>
		name의 오름차순, 내림차순 정렬 리스트를 각각 출력
	</p>
	
	<h2>name의 오름차순</h2>
	<ul>
		<c:forEach items="${list.stream().sorted((x,y) -> x.name_j.compareTo(y.name_j)).
			map(x->String.format('%s(%s)',x.name_j,x.age)).toList()}" var="l">
			<li>${l }</li>
		</c:forEach>	
	</ul>
		
	<h2>name의 내림차순</h2>
	<ul>
		<c:forEach items="${list.stream().sorted((x,y) -> y.name_j.compareTo(x.name_j)).
			map(x->String.format('%s(%s)',x.name_j,x.age)).toList()}" var="l">
			<li>${l }</li>
		</c:forEach>
	</ul>
	
	
	
</body>
</html>