<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="lst" value="<%= java.util.Arrays.asList(1,2,3,4,5) %>" />
	<h1>${lst.stream().sum() }</h1>
	<h1>${list=[1,2,3,4,5]; list.stream().sum() }</h1>
	
	<c:forEach var="val" items="${list }">
		<c:set var="sum" value="${sum+val }"/>
	</c:forEach>
</body>
</html>