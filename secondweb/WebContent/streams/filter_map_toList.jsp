<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>STREAM 기본 메서드</h1>
	<p>List에 1,2,3,4,5,6,7 할당<br>
		이중 홀수만 추출(filter)<br>
		홀수를 3제곱한 리스트 만들기
		각각의 li에 각각의 값을 출력
	</p>
	
	<ul>
		<c:forEach items="${list=[1,2,3,4,5,6,7]; list.stream().filter(x -> x%2==1).map(x -> x*x*x).toList() }" var="l">
			<li>${l}</li>
		</c:forEach>
	</ul>
</body>
</html>