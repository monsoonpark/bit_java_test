<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>JSTL FOREACH - 컬렉션</h1>
	<% 
		ArrayList<Integer> ints=new ArrayList<>();
		ints.add(3);ints.add(2);ints.add(1);
		pageContext.setAttribute("ints", ints);
	%>
	<c:forEach items="${ints }" var="i"> 
		<h${i }>NUMBER</h${i }>
	</c:forEach>
	
	<h1>JSTL FOREACH</h1>
	<c:forEach var="i" begin="1" end="9" step="1">
		<h1>${i }단</h1>
		<c:forEach var="j" begin="1" end="9" step="1">
			${i } x ${j } = ${i*j } <br>
		</c:forEach>
	</c:forEach>
	
	
	
</body>
</html>