<%@page import="bits.Test_Member"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>jstl_foreach_exe.jsp</title>
</head>
<body>
	<%
		//ArralyList
		ArrayList<Test_Member> tms=new ArrayList<>();
		tms.add(new Test_Member("아이유",25));
		tms.add(new Test_Member("수지",25));
		tms.add(new Test_Member("아이린",28));
		
		//setAttribute
		pageContext.setAttribute("tms",tms);
		
		//tms를 jstl을 이용해 <li>아이유(25세)</li>로 출력
	%>
	<ul>
		<c:forEach items="${tms }" var="i" >
			<li><c:out value="${i.name }"/>(<c:out value="${i.age }"/>)</li>
			<li>${i.name }(${i.age })</li>
		</c:forEach>
	</ul>
</body>
</html>