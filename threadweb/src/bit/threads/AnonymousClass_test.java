package bit.threads;

//무명클래스
public class AnonymousClass_test {

	public static void main(String[] args) {
		//초기
		Thread alpha=new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 74; i > 64; i--) {
					System.out.printf("Anonymous_alpha : %s %n", (char) i);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		
		Thread num= new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 10; i > 0; i--) {
					System.out.printf("Anonymous_num : %d %n", i);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
		});
		
		//쓰레드 실행
		alpha.start();
		num.start();
	}

}
