package bit.threads;

public class Lambda_test {

	public static void main(String[] args) {
		//lambda 활용
		Thread alpha=new Thread(()-> {
			for (int i = 74; i > 64; i--) {
				System.out.printf("Labda_alpha : %s %n", (char) i);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		});
		
		Thread num=new Thread(()-> {
			for (int i = 10; i > 0; i--) {
				System.out.printf("Labda_num : %d %n", i);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		//Thread 생성
		alpha.start();
		num.start();

		//join 쓰레드 종료전 호출 쓰레드를 끝내지 않음
		try {
			alpha.join();
			num.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		//메세지 출력
		System.out.println("MAIN THREAD END");
		
	}

}
