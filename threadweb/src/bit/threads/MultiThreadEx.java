package bit.threads;

public class MultiThreadEx implements Runnable{
	@Override
	public void run() {
		int f=0;
		int s=0;
		for(int i=0; i<20; i++) {
			f++;
			s++;
			System.out.printf("first: %d,",f);
			System.out.printf("second: %d,",s);
			System.out.printf("쓰레드: %s, %n",Thread.currentThread().getName());
			
		}
		
	}

	public static void main(String[] args) {
		MultiThreadEx str1=new MultiThreadEx();
		Thread fThread=new Thread(str1, "첫번째 쓰레드");
		fThread.start();
		
		MultiThreadEx str2=new MultiThreadEx();
		Thread sThread=new Thread(str2, "두번째 쓰레드");
		sThread.start();
		

	}


}
