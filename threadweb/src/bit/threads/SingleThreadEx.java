package bit.threads;

public class SingleThreadEx extends Thread{
	private int[] temp;
	
	public SingleThreadEx(String threadname) {
		temp=new int[10];
		for(int i=0; i<temp.length; i++) {
			temp[i]	= i;
		}
	}
	
	public void run() {
		for( int i: temp) {
			try {
					sleep(1000);
			}catch(InterruptedException ie) {
				ie.printStackTrace();
			}
			System.out.printf("쓰레드: %s",currentThread().getName());
			System.out.printf("temp value: %d %n",i);
		}
	}
	
	public static void main(String[] args) {
		SingleThreadEx st=new SingleThreadEx("첫번쩨");
		st.start();
	}
}
