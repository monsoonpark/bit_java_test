package bit.threads;

public class Runnable_test {

	public static void main(String[] args) {
		// Runnable 자식을 사용한 Thread
		Thread alpha=new Thread(new Runnable_prints(true));
		Thread num=new Thread(new Runnable_prints(false));
		
		alpha.start();
		num.start();

	}

}
