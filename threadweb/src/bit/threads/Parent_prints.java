package bit.threads;

public class Parent_prints {

	// 메서드
	public void Print_alpha() {
		for (int i = 74; i > 64; i--) {
			System.out.printf("Thread_alpha : %s %n", (char) i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	// 메서드
	public void Print_num() {
		for (int i = 10; i > 0; i--) {
			System.out.printf("Thread_num : %d %n", i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
