package bit.threads;

//Thread 클래스를 상속
public class Thread_num extends Thread{
	
	//Thread의 시작 메서드
	@Override
	public void run() {
		super.run();
		Print_num();
	}
	
	//메서드
	public void Print_num() {
		for (int i = 10; i > 0; i--) {
			System.out.printf("Thread_num : %d %n",i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
