package bit.threads;

public class SingleRunnableEx implements Runnable{
	private int temp[];
	
	public SingleRunnableEx() {
		temp=new int[10];
		for(int i=0; i<10; i++) {
			temp[i]=i;
		}
	}
	@Override
	public void run() {
		for( int i: temp) {
			try {
					Thread.sleep(1000);
			}catch(InterruptedException ie) {
				ie.printStackTrace();
			}
			System.out.printf("쓰레드: %s",Thread.currentThread().getName());
			System.out.printf("temp value: %d %n",i);
		}
		
	}

	public static void main(String[] args) {
		SingleThreadEx st=new SingleThreadEx("첫번쩨");
		st.start();
	}
	
}
