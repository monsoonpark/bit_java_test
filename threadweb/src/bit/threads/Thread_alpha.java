package bit.threads;

//Thread 클래스를 상속
public class Thread_alpha extends Thread{
	
	//Thread의 시작 메서드
	@Override
	public void run() {
		super.run();
		Print_alpha();
	}
	
	//메서드
	public void Print_alpha() {
		for (int i = 74; i > 64; i--) {
			System.out.printf("Thread_alpha : %s %n",(char)i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
