package bit.threads;

public class Runnable_prints extends Parent_prints implements Runnable {

	//멤버 변수
	boolean isAlpah;
	
	//생성자
	public Runnable_prints(boolean isAlpah) {
		this.isAlpah = isAlpah;
	}
	
	@Override
	public void run() {
		if(isAlpah) {
			this.Print_alpha();
		}else {
			this.Print_num();;
		}
	}

}
