<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>index.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	
	window.onload=function(){
		//alert(document.getElementById("ulDom"));
		var ul=document.getElementById("ulDom");
		//li를 클릭하면 li 엘리먼트 제거
		//alert(ul.getElementsByTagName('li'));
		for (var i = 0; i < ul.getElementsByTagName('li').length; i++) {
			ul.getElementsByTagName('li')[i].onclick=function(){
				//alert(this);
				this.remove();
			}
		}
		document.getElementById("dom").onclick=function(){
			for (var i = 0; i < ul.getElementsByTagName('li').length; i++) {
				ul.getElementsByTagName('li')[i].show();
			}
		}
	}
	
	//JQUERY인 경우
	$(function(){
		$('#ulJquery li').click(function(){
			//this.remove();
			//alert(this);	//this = javascript
			$(this).hide();
			//alert($(this));	//$(this) = jquery
		});
		//alert($('#ulDom li'));
		$('#jquery').click(function(){
			$('#ulJquery li').show();
		});
		
	});
</script>
</head>
<body>
	<h1>DOM SCRIPT VS JQUERY</h1>
	<h3 id="dom">DOM SCRIPT</h3>
	<ul id="ulDom">
		<li>LI DOM</li>
		<li>LI DOM</li>
		<li>LI DOM</li>
		
	</ul>
	
	<h3 id="jquery">JQUERY</h3>
	<ul id="ulJquery">
		<li>JQUERY</li>
		<li>JQUERY</li>
		<li>JQUERY</li>
		
	</ul>
	<div>
	</div>
</body>
</html>