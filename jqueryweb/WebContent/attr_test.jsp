<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>attr_test.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	/* 
	
	*/
	//초기
	file_infos=[{name:'파일1',src:'img01.jpg'},
				{name:'파일2',src:'img02.jpg'},
				{name:'파일3',src:'img03.jpg'},
				{name:'파일4',src:'img04.jpg'},
				{name:'파일5',src:'img05.jpg'},
				{name:'파일6',src:'sun.gif'}];
	//alert(file_infos[0].name);
	//alert(file_infos[0].src);
	
	//프로그램 시작되면 img를 클릭할 경우..
	//file_infos의 이미가 랜덤하게 img태그에 출력
	//jquery attr 메서드 사용 
	$(function(){
		$('#target_img').click(function(){
			//var random = Math.floor(Math.random() * 5) + 1;
			//var random = Math.ceil(Math.random() * 5);
			var i=parseInt(Math.random()*file_infos.length);
			$(this).attr('alt',file_infos[i].name);
			$(this).attr('src',"imgs/"+file_infos[i].src);
			
		});
	});

</script>
</head>
<body>
	<h1>H1</h1>

	<ul>
		<li>
			<img id="target_img" alt="test" src="imgs/actor01.jpg" style="width:300px; height:200px;">
		</li>
		
	</ul>
	
</body>
</html>