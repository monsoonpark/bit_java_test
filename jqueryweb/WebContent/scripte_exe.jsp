<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>jquerytemplate.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	//입력후 결과를 랜덤하게 확인하는 페이지
	var x,y;
	$(function(){
		$('#test').click(function(){
			input=$('#input').val();
			if(input!="" && input>0 && input<10){
				x=$('#input').val();
				y=Math.ceil(Math.random()*9);
				alert((x+" x "+y+" = ?"));
				$('#input').attr("value",(x+" x "+y+" = ?"));
			}
		});
		$('#check').click(function(){
			result=$('#result').val();
			if(result!=""){
				if(result==x*y){
					alert("정답입니다.");
					$('#input').attr("value","");
					$('#result').attr("value","");
				}else{
					alert("틀렸습니다.");
				}
			}
		});
	});	

</script>
<style>
	li{list-style: none;}
</style>
</head>
<body>
	<h1>구구단 테스트</h1>
	<form id="frm">
		<ul>
			<li> <input id="input" type="text" placeholder="원하는 단 입력후 test버튼" /></li>
			<li> <input id="result" type="text" placeholder="정답 입력후 확인버튼"/></li>
			<li>
				<button id="test" type="button">test</button>
				<button id="check" type="button">확인</button>
			</li>
		</ul>
	</form>
	
	
</body>
</html>