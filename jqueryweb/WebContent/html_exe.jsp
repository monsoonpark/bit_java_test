<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>html_exe.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	file_infos=[{name:'파일1',src:'img01.jpg'},
		{name:'파일2',src:'img02.jpg'},
		{name:'파일3',src:'img03.jpg'},
		{name:'파일4',src:'img04.jpg'},
		{name:'파일5',src:'img05.jpg'}];
	/*
		프로그램 시작되면
		filte_infos의 이미지를 나열
		나열된 이미지를 클릭하면
		ul_selectedImgs에 선택된 이미지를 li로 추가
	*/
	
	$(function(){	
		/*
		for ( var i in file_infos) {
			e=file_infos[i];
			$("#ul_selectedImgs").append("<li><img alt='"+e.name+"' src='imgs/"+e.src+"'/></li>")
		}
		*/
		$("#ul_thumbnail li").click(function(){
			img=$(this).html();
			$("#ul_selectedImgs").append("<li>"+img+"</li>");
		});
	});	
	function addLi(){
		$("#ul_selectedImgs").append(this);
	}

</script>
<style type="text/css">
	#ul_selectedImgs{
		list-style: none;
	}
	#ul_selectedImgs li{
		display: inline-block;
	}
	#ul_selectedImgs li img{
		border: dotted thick tan;
	}
</style>
</head>
<body>
	<h1>h1</h1>
	<ul id="ul_thumbnail">
		<li onclick="addLi()"><img src="imgs/img01.jpg"/></li>
	</ul>
	
	<ul id="ul_selectedImgs">
		<li></li>
	</ul>
	
</body>
</html>