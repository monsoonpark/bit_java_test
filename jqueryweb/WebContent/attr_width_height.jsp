<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>attr_width_height.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#btn_bigger').click(function(){
			w=$('#target_img').attr("width")*1.1; 
			h=$('#target_img').attr("height")*1.1;
			resizeImg(w,h );
		});
		$('#btn_smaller').click(function(){
			w=$('#target_img').attr("width")*0.9;
			h=$('#target_img').attr("height")*0.9;
			resizeImg(w,h );
	});
	});
	function resizeImg(w,h){
		$('#target_img').attr("width",w);
		$('#target_img').attr("height",h);
	}
</script>
</head>
<body>
	<h1></h1>
	<ul>
		<li>
			<button id="btn_bigger">이미지 키우기</button>
			<button id="btn_smaller">이미지 줄이기</button>
			
		</li>
		<li>
			<img id="target_img" alt="test" src="imgs/actor01.jpg" 
			width="300" height="200">
		</li>
		
	</ul>
</body>
</html>