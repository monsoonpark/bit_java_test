<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>append_element.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	file_infos=[{name:'파일1',src:'img01.jpg'},
				{name:'파일2',src:'img02.jpg'},
				{name:'파일3',src:'img03.jpg'},
				{name:'파일4',src:'img04.jpg'},
				{name:'파일5',src:'img05.jpg'}];
	/*append test*/
	/*
	$(function(){
		$('#ul_apptest').append($('<li><img alt="test" src="imgs/sun.gif"/></li>'));
		
	});
	*/
	/*프로그램이 시작 file_infos의 이미지를 ul_apptest에 li와 함께 추가
	  각각의 li는 클릭시 제거됨.
	*/
	$(function(){
		/*
		for (var i = 0; i < file_infos.length; i++) {
			$('#ul_apptest').append($('<li>'+
			'<img alt="'+file_infos[i].name+'" src="imgs/'+file_infos[i].src+'"/></li>'));
		}
		$('#ul_apptest li').click(function(){
			$(this).hide();
		});
		$('#show').click(function(){
			$('#ul_apptest li').show();
		});
		*/
		
		for ( var i in file_infos) {
			e=$('<li><img alt="'+file_infos[i].name+'" src="imgs/'+file_infos[i].src+'"/></li>');
			e.click(function(){
				$(this).hide();
			});
			$('#ul_apptest').append(e);
		}
		$('#show').click(function(){
			$('#ul_apptest li').show();
		});
		
	});	

</script>
<style>
	img{width:150px; height:100px; border:thick solid gold;}
</style>
</head>
<body>
	<h1 id="show">APPEND TEST</h1>

	<ul id="ul_apptest">
	
	</ul>
	
</body>
</html>