<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>jquerybasic_test.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	/*
		1.html:
				form태그에 이미지파일 이름, 넓이, 높이, 제목을 입력받고
				생성버튼을 클릭하면
				특점 li에 img 엘리먼트가 append
	
	*/
	file_infos=[{name:'파일1',src:'img01.jpg'},
		{name:'파일2',src:'img02.jpg'},
		{name:'파일3',src:'img03.jpg'},
		{name:'파일4',src:'img04.jpg'},
		{name:'파일5',src:'img05.jpg'}];
		
  $(function(){
	   // 로드시 select의 option추가
	   sel = $("#sel_filenames");
	   for ( var i in file_infos) {
	    e = file_infos[i];
	    // 옵션생성
	    opt = $("<option value='"+ e.src +"'>"+ e.name +"</option>");
	    sel.append(opt);
	   }
	   
	   // 버튼에 이벤트추가
	   $("#btnAdd").click(function(){
	    // 정보입력 여부 확인
	    var txt_width = $("#txt_width").val(); 
	    var txt_height = $("#txt_height").val(); 
	    var txt_title = $("#txt_title").val(); 
	    var sel_filenames = $("#sel_filenames").val(); 
	    if(txt_width == ""
	      || txt_height == ""
	      || txt_title == ""){
	     alert("모두입력해주세요!"); return;
	    }
	    
	    e = $("<li><img src='imgs/"+ sel_filenames
	      +"' width='"+ txt_width 
	      +"' height='"+ txt_height 
	      +"' title='"+ txt_title 
	      +"' /></li>");
	    $("#ul_imgslist").append(e);
	   });
	  });

</script>
</head>
<body>
	<h1>img입력</h1>

	<form id="frm_imgs">
		<ul>
			<li>이미지 파일이름 : <select id="sel_filenames"></select></li>
			<li>넓이 : <input type="text" id="txt_width" /></li>
			<li>높이 : <input type="text" id="txt_height" /></li>
			<li>제목 : <input type="text" id="txt_title" /></li>
			<li>
				<button id="btnAdd" type="button">이미지 추가</button>
			</li>
		</ul>
	</form>
	<h1>IMAGE LIST</h1>
	<ul id="ul_imgslist"></ul>

</body>
</html>