<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>text_test.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	/*
		ul_navs의 li클릭시 li의 태그안의 text를 
		ul_selectedText의 li로 추가
	*/
	$(function(){
		$('#ul_navs li').click(function(){
			text=$(this).find(">:first-child").text();
			//text=$(this).text();
			$('#ul_selectedText').append("<li>"+text+"</li>");
		});
	});
</script>
</head>
<body>
	<h1></h1>
	<ul id="ul_navs">
		<li><p>P태그의 TEXT</p></li>
		<li><h1>H1의 TEXT</h1></li>
		<li><h2>H2의 TEXT</h2></li>
		<li><h3>H3의 TEXT</h3></li>
		<li><span><i>ITALIC</i>SPAN</span></li>
		<li><div>DIV의 TEXT</div></li>
	</ul>
	
	<h1>TEXT LIST</h1>
	<ul id="ul_selectedText">
		
	</ul>
	
</body>
</html>