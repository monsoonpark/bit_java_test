<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>jquerytemplate.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	file_infos=[{name:'파일1',src:'img01.jpg'},
		{name:'파일2',src:'img02.jpg'},
		{name:'파일3',src:'img03.jpg'},
		{name:'파일4',src:'img04.jpg'},
		{name:'파일5',src:'img05.jpg'}];
	$(function(){
		for ( var i in file_infos) {
			btn=$("<button>SHOW / HIDE</button>");
			btn.click(function(){
				//$(e).hide();
				//$(this).prev().prev().toggle(1000);
				//$(this).prev().prev().fadeToggle(1000);
				$(this).prev().prev().slideToggle(1000);
			});
			e=$('<li><img src="imgs/'+file_infos[i].src+'" /><br/></li>');
			e.append(btn);
			$('#ul_imgList').append(e);
		}
		$('#show').click(function(){
			$('#ul_apptest li').show();
		});
		
	});	

</script>
</head>
<body>
	<h1>h1</h1>
	
	<ul id="ul_imgList">
		<li>	</li>
	</ul>
	
</body>
</html>