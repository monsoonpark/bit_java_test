<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>animate_test.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){	//onload역할
		//li click
		$("#ul_target li").click(function(){
			$(this).animate({
				opacity: '0.5',
				width: '110px',
				height: '110px'
			});
		});
	});	

</script>
<style type="text/css">
	#ul_target{ list-style: none; }
	#ul_target li{ display: inline-block; width:100px; height:100px;
		text-align: center; border:thick solid gold;		
	}
	#ul_target li:nth-of-type(1){ }
</style>
</head>
<body>
	<h1>h1</h1>

	<ul id="ul_target">
		<li>LI ITEM</li>
		<li>LI ITEM</li>
		<li>LI ITEM</li>
		<li>LI ITEM</li>
		<li>LI ITEM</li>
		
	</ul>
	
</body>
</html>