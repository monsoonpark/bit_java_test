<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>html_test.jsp</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	/*	*/
	$(function(){
		//ul_target의 첫번째 li
		$("#ul_target li:nth-of-type(1)").html("<h1>TOP</h1>");
		//ul_target의 두번째 li
		$("#ul_target li:nth-of-type(2)").html("<img src='imgs/img01.jpg'/>");
		//ul_target의 세번째 li
		$("#ul_target li:nth-of-type(3)").html("<hr/><p>ALL RIGHTS RESERVED &COPY;</p>");
		
		$("#ul_btns li button").click(function(){
			alert($("#ul_target li:nth-of-type(1)").html());
			alert($("#ul_target li:nth-of-type(2)").html());
			alert($("#ul_target li:nth-of-type(3)").html());
		});
	});

</script>
</head>
<body>
	<h1>html GET</h1>
	<ul id="ul_btns">
		<li><button>클릭시 ul_target의 안쪽 html을 alert</button></li>
	</ul>
	
	<h1>html METHOD</h1>
	<ul id="ul_target">
		<li>	</li>
		<li>	</li>
		<li>	</li>
		
	</ul>
	
</body>
</html>