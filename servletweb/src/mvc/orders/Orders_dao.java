package mvc.orders;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;

import the.bits.Bitdb;

public class Orders_dao {
		//멤버변수 
		private static Orders_dao dao;
		//static 블럭
		static {
			dao=new Orders_dao();
		}
		//생성자
		private Orders_dao() {
		}
		//getInstance();
		public static Orders_dao getInstance(){
			return dao;
		}
		
		public ArrayList<HashMap<String,String>> orderList() {
			//초기
			ArrayList<HashMap<String,String>> list=new ArrayList<>();
			HashMap<String,String> hm=null;
			//select
			ResultSet rs=Bitdb.executeQuery("select * from bookorder");
			//rs=>ArrayList<HashMap<String,String>>
			try {
				ResultSetMetaData md=rs.getMetaData();
				while(rs.next()) {
					hm=new HashMap<>();
					for (int i = 1; i < md.getColumnCount()+1; i++) {
						hm.put(md.getColumnName(i), rs.getString(i));
					}
					list.add(hm);
				}
				
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				try {
					Bitdb.close();
				}catch(Exception e) {
					
				}
			}
			return list;
		}
}
