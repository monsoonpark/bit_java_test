package mvc.orders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.command.CommandHandler;

public class OrderListHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Orders_service.bindOrders(req);;
		return "/WEB-INF/orders/order_list.jsp";
	}

}
