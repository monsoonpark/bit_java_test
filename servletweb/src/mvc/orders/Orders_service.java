package mvc.orders;

import javax.servlet.http.HttpServletRequest;


public class Orders_service {

	static Orders_dao dao;
	static {
		dao=Orders_dao.getInstance();
	}
	//
	public static void bindOrders(HttpServletRequest r) {
		r.setAttribute("list", dao.orderList());
		
	}
}
