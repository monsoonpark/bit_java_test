package bit.services;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import bit.dao.Godao;
import bit.servlets.GoController;

public class GoService {
	//멤버변수
	static Godao dao;
	static {
		dao=Godao.getInstance();
	}
	//
	public static void bindAddrs(HttpServletRequest r) {
		r.setAttribute("list", dao.selectList());
		
	}
	
	public static void bindBook(HttpServletRequest r) {
		r.setAttribute("list", dao.booklist());
		
	}
	
	
	
	
}
