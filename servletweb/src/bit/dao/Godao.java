package bit.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import the.bits.Bitdb;

public class Godao {
	//멤버변수 
	private static Godao dao;
	//static 블럭
	static {
		dao=new Godao();
	}
	//생성자
	private Godao() {
	}
	//getInstance();
	public static Godao getInstance(){
		return dao;
	}
	
	//SELECT addrs
	public ArrayList<HashMap<String,String>> selectList() {
		//초기
		ArrayList<HashMap<String,String>> list=new ArrayList<>();
		HashMap<String,String> hm=null;
		//select
		ResultSet rs=Bitdb.executeQuery("select * from addrs order by aid desc");
		//rs=>ArrayList<HashMap<String,String>>
		try {
			ResultSetMetaData md=rs.getMetaData();
			while(rs.next()) {
				hm=new HashMap<>();
				for (int i = 1; i < md.getColumnCount(); i++) {
					hm.put(md.getColumnName(i), rs.getString(i));
				}
				list.add(hm);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				Bitdb.close();
			}catch(Exception e) {
				
			}
		}
		return list;
	}
	
	public ArrayList<HashMap<String,String>> booklist() {
		//초기
		ArrayList<HashMap<String,String>> list=new ArrayList<>();
		HashMap<String,String> hm=null;
		//select
		ResultSet rs=Bitdb.executeQuery("select * from book order by bookid desc");
		//rs=>ArrayList<HashMap<String,String>>
		try {
			ResultSetMetaData md=rs.getMetaData();
			while(rs.next()) {
				hm=new HashMap<>();
				for (int i = 1; i < md.getColumnCount(); i++) {
					hm.put(md.getColumnName(i), rs.getString(i));
				}
				list.add(hm);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				Bitdb.close();
			}catch(Exception e) {
				
			}
		}
		return list;
	}
	
	

}
