package bit.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bit.services.Service;

/**
 * Servlet implementation class Controller
 */
@WebServlet("*.do")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//요청확인
		System.out.println("request.getContextPath(): "+request.getContextPath());
		System.out.println("request.getRequestURI(): "+request.getRequestURI());
		System.out.println("request.getRequestURL(): "+request.getRequestURL());
		
		//초기
		Service s=new Service();
		
		//요청구분
		String uri=request.getRequestURI();
		uri=uri.substring(uri.lastIndexOf("/")+1, uri.indexOf("."));
		System.out.println("uri: "+uri);
		
		//요청별 처리
		switch(uri) {
			case "ajy": request.setAttribute("list",s.returnModelA() );
				break;
			case "books": request.setAttribute("list", s.returnModelB());
				break;
		}
		
		//forward처리
		request.getRequestDispatcher(uri+".jsp").forward(request, response);;
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
