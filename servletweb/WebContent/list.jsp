<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="list.go" method="post">
		<button>이동</button>
	</form>
	<ul>
		<c:forEach items="${list }" var="l" >
				<li> ${l.AID }(${l.ZCODE }).${l.ADDR}</li>
		</c:forEach>	
	</ul>
</body>
</html>