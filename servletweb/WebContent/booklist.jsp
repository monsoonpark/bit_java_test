<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="booklist.go" method="post">
		<button>이동</button>
	</form>
	<table border="1">
		<c:forEach items="${list }" var="li">
			<tr>
				<c:forEach items="${li }" var="l">
					<td>${l.value }</td>
				</c:forEach>
			</tr>
		</c:forEach>
	</table>
</body>
</html>