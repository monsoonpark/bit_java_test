package the.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class TransferTotest {

	public static void copyFile(File sourceFile, File destFile)throws IOException{
		FileChannel source=null;
		FileChannel destination=null;
		
		try {
			source=new FileInputStream(sourceFile).getChannel();
			destination=new FileOutputStream(destFile).getChannel();
			source.transferTo(0, source.size(), destination);
		}finally {
			if(source!=null) {
				source.close();
			}
			if(destination!=null) {
				destination.close();
			}
		}
	}
	
	public static void main(String[] args) {
		
		try {
			copyFile(new File("./src/the/tests/nio-data.txt"), new File("./src/the/tests/nio-data2.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
