package the.tests;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Channeltest {

	public static void main(String[] args) throws IOException {
		//초기
		RandomAccessFile afile=new RandomAccessFile(
				new File("./src/the/tests/nio-data.txt").getCanonicalPath(), "rw");
		//채널 생성
		FileChannel inChannel=afile.getChannel();
		//버퍼세팅
		ByteBuffer buf=ByteBuffer.allocate(48);
		int bytesRead=inChannel.read(buf);
		//버퍼의 데이터 출력
		while(bytesRead!=-1) {
			System.out.println("Read " + bytesRead);
			// 버퍼를 쓰기모드에서 읽기모드로 전환
			buf.flip();
			System.out.println("buf.flip()후 limit : " + buf.limit());
			// while
			while (buf.hasRemaining()) {
				System.out.print((char)buf.get());  // 한번에 1byte씩 리턴
			}
			System.out.println();
			buf.rewind();
			while (buf.hasRemaining()) {
				System.out.print((char) buf.get());
				System.out.print("-limit : " + buf.limit() + ",");
			}
			System.out.println();
			// 버퍼를 읽기모드에서 쓰기모드로 전환
			buf.clear();
			System.out.println("buf.clear()후 limit : " + buf.limit());
			bytesRead = inChannel.read(buf);
		}
		afile.close();
	}

}
