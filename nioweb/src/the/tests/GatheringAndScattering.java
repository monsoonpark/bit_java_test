package the.tests;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.ScatteringByteChannel;
import java.nio.charset.Charset;

public class GatheringAndScattering {
	// 멤버변수
	private static String FILENAME = "./src/the/tests/sample.txt";
	private static Charset charset = Charset.forName("UTF-8");

	// 메서드
	private static void scattering() {
		// Capacity 지정
		ByteBuffer bblen1 = ByteBuffer.allocate(1024);
		ByteBuffer bblen2 = ByteBuffer.allocate(1024);

		// 데이터 버퍼 선언
		ByteBuffer bbdata1 = null;
		ByteBuffer bbdata2 = null;

		// 데이터 가져오기
		FileInputStream in;
		try {
			in = new FileInputStream(FILENAME);
			ScatteringByteChannel scatter = in.getChannel();

			// 2개의 버퍼를 읽음
			scatter.read(new ByteBuffer[] { bblen1, bblen2 });

			// 포지션을 0으로 셋팅
			bblen1.position(0);
			bblen2.position(0);

			// ByteBuffer를 IntBuffer로 변환
			int len1 = bblen1.asIntBuffer().get();
			int len2 = bblen2.asIntBuffer().get();

			// len 확인
			System.out.println("Scattering : Len1= " + len1);
			System.out.println("Scattering : Len2= " + len2);

			bbdata1 = ByteBuffer.allocate(len1);
			bbdata2 = ByteBuffer.allocate(len2);

			// 채널로 부터 데이터 읽어오기
			scatter.read(new ByteBuffer[] { bbdata1, bbdata2 });

		} catch (FileNotFoundException exObj) {
			exObj.printStackTrace();
		} catch (IOException ioObj) {
			ioObj.printStackTrace();
		}

		// 스트링으로 변환
		String data1 = new String(bbdata1.array(), charset);
		String data2 = new String(bbdata2.array(), charset);

		System.out.println(data1);
		System.out.println(data2);
	}

	private static void gathering(String data1, String data2) {
		// Capacity 지정
		ByteBuffer bblen1 = ByteBuffer.allocate(1024);
		ByteBuffer bblen2 = ByteBuffer.allocate(1024);

		// 데이터를 버퍼에 기록
		ByteBuffer bbdata1 = ByteBuffer.wrap(data1.getBytes());
		ByteBuffer bbdata2 = ByteBuffer.wrap(data2.getBytes());

		int len1 = data1.length();
		int len2 = data2.length();

		// ByteBuffer를 IntBuffer로 변환
		bblen1.asIntBuffer().put(len1);
		bblen2.asIntBuffer().put(len2);

		System.out.println("Gathering : Len1= " + len1);
		System.out.println("Gathering : Len2= " + len2);

		// 파일에 쓰기
		try {
			FileOutputStream out = new FileOutputStream(FILENAME);
			GatheringByteChannel gather = out.getChannel();
			gather.write(new ByteBuffer[] { bblen1, bblen2, bbdata1, bbdata2 });
			out.close();
			gather.close();
		} catch (FileNotFoundException exObj) {
			exObj.printStackTrace();
		} catch (IOException ioObj) {
			ioObj.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String data1 = "1A 가나다라channel that can write bytes from a sequence of buffers.";
		String data2 = "23A 가나다라channel that can read bytes into a sequence of buffers.";

		// We are going to store 2 data's to the file using GatheringByteChannel
		gathering(data1, data2);

		scattering();
	}

}
