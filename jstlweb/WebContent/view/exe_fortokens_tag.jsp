<h1>FORTOKENS 예제</h1>
<%
	String strs="램페이지-그날바다,콰이어트 플레이스.나를 기억해=바람바람바람";
	//strs를 li로 출력
	pageContext.setAttribute("strs", strs);
	//이중 forTokens
	String strs2="램페이지-미국-액션,그날바다:한국:다큐멘터리.레디플레이어원=미국=액션";
	pageContext.setAttribute("strs2", strs2);
%>

<c:forTokens var="token" items="${strs }" delims="-,.=">
	<li>${token }
</c:forTokens>

<style>
	table ,tr , td{
		border: 1px black solid;
		border-collapse: collapse;
	}
</style>

<table>
<c:forTokens var="t" items="${strs2 }" delims=",.">
	<tr>
		<c:forTokens var="t2" items="${t }" delims="-:=">
			<td>${t2 }</td>	
		</c:forTokens>
	</tr>
</c:forTokens>
</table>
