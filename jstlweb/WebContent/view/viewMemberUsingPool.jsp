<%@page import="the.test.Jstl_member"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Objects"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>

	
	<%
			Connection conn=null;
			Statement stmt=null;
			ResultSet rs=null;
		try{
			String jdbcDriver="jdbc:apache:commons:dbcp:jstlweb";		
			conn=DriverManager.getConnection(jdbcDriver);
			if(Objects.isNull(conn) || conn.isClosed()){
				throw new Exception("DB연결 실패");
			}
			
			stmt=conn.createStatement();
			String query="select * from jstl_member order by mid";
			rs=stmt.executeQuery(query);
			Jstl_member jm=null;
			ArrayList<Jstl_member> list=new ArrayList<>();
			while(rs.next()){
				jm=new Jstl_member();
				jm.setMname(rs.getString("mname"));
				jm.setMuserid(rs.getString("muserid"));
				jm.setMip(rs.getString("mip"));
				list.add(jm);
			}
			pageContext.setAttribute("list", list);
		}catch(Exception ex){
			System.out.println("execute 예외: "+ex.getMessage());
		}finally{
			if(rs!=null){
				try{
					rs.close();
				}catch(SQLException e){}
			}
			if(stmt!=null){
				try{
					rs.close();
				}catch(SQLException e){}
			}
			if(conn!=null){
				try{
					rs.close();
				}catch(SQLException e){}
			}
		}
	%>
<table width="400px" border="1">
	<tr>
		<td>이름</td><td>아이디</td><td>아이피</td>
	</tr>
		<c:forEach items="${list }" var="j">
			<tr>
				<td>${j.mname }</td>
				<td>${j.muserid }</td>
				<td>${j.mip }</td>
			</tr>
		</c:forEach>
</table>