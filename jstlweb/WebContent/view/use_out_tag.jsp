<%@page import="java.io.IOException, java.io.FileReader"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	FileReader reader=null;
	
	try{
		//String path=request.getParameter("path");
		String path="/view/use_if_tag.jsp";
		reader=new FileReader(getServletContext().getRealPath(path));
%>
<pre>
	소스코드: <%=path %>
	<c:out value="<%=reader %>" escapeXml="true"/>
</pre>
<%
	}catch(IOException ex){
%>
	에러:<%=ex.getMessage() %>
	
<%
	}finally{
		if(reader!=null){
			try{
				reader.close();
			}catch(IOException ex){
			}
		}
	}
%>