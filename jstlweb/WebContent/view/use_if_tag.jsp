
<script>
	function initMap() {
		var mapPos = {
			lat : 37.499481,
			lng : 127.029330
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom : 18,
			center : mapPos
		});
		var marker = new google.maps.Marker({
			position : mapPos,
			map : map
		});
	}
</script>

<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBr1WbmiuFZ-W5lRxTbZSMuN8TpZdU3mUs&callback=initMap"
	async defer></script>
<style>
#map {
	width: 100%;
	height: 400px;
	background-color: whitegrey;
}
</style>
<head>
<link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css">
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>
<h3>My Google Maps Demo</h3>
<div id="map"></div>

