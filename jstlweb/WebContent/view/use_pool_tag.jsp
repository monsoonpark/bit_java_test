<%@page import="the.test.Code"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.Objects"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<h1>CONNECTION POOLING TEST</h1>
<%
 Connection c = null;
 Statement s = null;
 ResultSet rs = null;
 try {
  c = DriverManager.getConnection("jdbc:apache:commons:dbcp:jstlweb");
  if (Objects.isNull(c) || c.isClosed()) {
   throw new Exception("DB연결 예외");
  }
  s = c.createStatement();
  // select
  rs = s.executeQuery("SELECT * FROM TEST_CNATIONS ORDER BY NCODE DESC");
  Code code = null;
  ArrayList<Code> list = new ArrayList<>();
  while (rs.next()) {
   code = new Code(rs.getString(2), rs.getInt(1));
   list.add(code);
  }
  // setAttribute
  pageContext.setAttribute("list", list);
 } catch (Exception e) {
  System.out.printf("execute 예외 : %s ", e.getMessage());
 } finally {
  if (!Objects.isNull(rs))
   rs.close();
  if (!Objects.isNull(s))
   s.close();
  if (!Objects.isNull(c))
   c.close();
 }
%>
<h1>LIST</h1>
<ul>
 <c:forEach items="${list}" var="c">
  <li>${c.code}.${c.name}</li>
 </c:forEach>
</ul>