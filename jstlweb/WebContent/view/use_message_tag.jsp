<!-- <fmt:setLocale value="en"/> -->
<!-- <fmt:bundle basename="resource.message"> -->

	<fmt:message key="TITLE" var="title"></fmt:message>
	<h1>${title }</h1>
	<p>
		<fmt:message key="GREETING"></fmt:message>
	</p>   
	<hr/>
	<c:if test="${!empty param.id }">	
		<fmt:message key="VISITOR">
			<fmt:param value="${param.id }"></fmt:param> 
		</fmt:message>
	</c:if>
<!-- </fmt:bundle> -->