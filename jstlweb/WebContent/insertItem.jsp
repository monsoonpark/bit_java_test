<%@page import="com.sun.xml.internal.bind.v2.schemagen.xmlschema.Occurs"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Objects"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<%
		String url = "jdbc:oracle:thin:@localhost:1521:orcl";
		String user = "scott";
		String pwd = "1234";
		Connection conn = null;
		PreparedStatement stmt = null;
		Class.forName("oracle.jdbc.driver.OracleDriver");
	%>
	<script type="text/javascript">
		function act() {
	<%conn = DriverManager.getConnection(url, user, pwd);
			if (Objects.isNull(conn) || conn.isClosed()) {
				throw new Exception("DB연결 실패");
			}
			try {
				conn = DriverManager.getConnection(url, user, pwd);
				conn.setAutoCommit(false);

				stmt = conn.prepareStatement("insert into item values(?,?)");
				stmt.setString(1, "seq_item.nextval");
				stmt.setString(2, "first");
				stmt.executeUpdate();

				if (request.getParameter("error") != null) {
					throw new Exception("의도적 익셉션");
				}
				stmt = conn.prepareStatement("insert into item values(?,?)");
				stmt.setString(1, "seq_item.nextval");
				stmt.setString(2, "second");
				stmt.executeUpdate();

				conn.commit();

			} catch (Throwable e) {
				if (conn != null) {
					try {
						conn.rollback();
					} catch (SQLException ex) {
					}
				}
			} finally {

			}%>
		alert("완료");
		}
	</script>

	<input type="button" value="등록" onclick="act()" />


</body>
</html>