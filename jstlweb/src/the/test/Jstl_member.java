package the.test;

import java.sql.Date;

public class Jstl_member {
	//
	int mid;
	String mname;
	String muserid;
	String mpwd;
	String mip;
	//
	public Jstl_member() {
		super();
	}
	public Jstl_member(int mid, String mname, String muserid, String mpwd, String mip) {
		super();
		this.mid = mid;
		this.mname = mname;
		this.muserid = muserid;
		this.mpwd = mpwd;
		this.mip = mip;
	}
	//
	public int getMid() {
		return mid;
	}
	public void setMid(int i) {
		this.mid = i;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getMuserid() {
		return muserid;
	}
	public void setMuserid(String muserid) {
		this.muserid = muserid;
	}
	public String getMpwd() {
		return mpwd;
	}
	public void setMpwd(String mpwd) {
		this.mpwd = mpwd;
	}
	public String getMip() {
		return mip;
	}
	public void setMip(String mip) {
		this.mip = mip;
	}
	
}

