
<style>
@import
	url(https://fonts.googleapis.com/css?family=Raleway:400,,800,900);

.titles .title {
	font-weight: 600;
	color: transparent;
	font-size: 100px;
	background:
		url("https://phandroid.s3.amazonaws.com/wp-content/uploads/2014/05/rainbow-nebula.jpg")
		repeat;
	background-position: 40% 50%;
	-webkit-background-clip: text;
	position: relative;
	text-align: center;
	line-height: 150px;
	letter-spacing: -8px;
}

.titles .subtitle {
	display: block;
	text-align: center;
	text-transform: uppercase;
	padding-top: 10px;
}
</style>
<div class="titles">
	<div class="container">
		<div class="title">Which is your favourite.</div>
		<div class="subtitle"></div>
	</div>
</div>
<script>
	$(document).ready(function() {
		var mouseX, mouseY;
		var ww = $(window).width();
		var wh = $(window).height();
		var traX, traY;
		$(document).mousemove(function(e) {
			mouseX = e.pageX;
			mouseY = e.pageY;
			traX = ((6 * mouseX) / 570) + 50;
			traY = ((6 * mouseY) / 570) + 50;
			console.log(traX);
			$(".title").css({
				"background-position" : traX + "%" + traY + "%"
			});
		});
	});
</script>