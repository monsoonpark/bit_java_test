<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<select class="form-control" id="ssort" name="ssort" style="width: 80px;">
	<option value="0">전체</option>
	<c:forEach items="${sortlist }" var="s">
		<c:choose>
			<c:when test="${s.scode eq sort }">
				<option value="${s.scode }" selected>${s.sname }</option>
			</c:when>
			<c:otherwise>
				<option value="${s.scode }">${s.sname }</option>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</select>

<input type="hidden" id="sort" name="sort" value="all"/>
<hr />
<table class="table table-striped table-hover" id="mainbbs">
	<thead>
		<tr>
			<td>글번호</td>
			<td>분류</td>
			<td>제목</td>
			<td><span class="glyphicon glyphicon-user"></span> 작성자</td>
			<td><span class="glyphicon glyphicon-time"></span> 날짜</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${maplist }" var="map">
			<tr>
				<td>${map.bid }</td>
				<td>${map.sname }</td>
				<td><a href="contentdetail.do?num=${map.bid}">${map.btitle }</a></td>
				<td>${map.uname }</td>
				<td>${map.bdate }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<c:if test="${empty login || login !='ok'}">
	<div style="float: right;" class="bbswrite">
		<a class="btn btn-primary" data-toggle="tooltip" title="글등록을 위해서 로그인하세요!" data-placement="left">
			<span class="glyphicon glyphicon-lock"></span> 글등록
		</a>
	</div>
</c:if>
<c:if test="${!empty login && login == 'ok'}">
	<div style="float: right;" class="bbswrite">
		<a href="bbswrite.do" class="btn btn-primary">
			<span class="glyphicon glyphicon-edit"></span> 글등록
		</a>
	</div>
</c:if>
<hr />
<div class="text-center">
	<ul class="pagination">
		<!-- 
		<li><a href="#"><span class="glyphicon glyphicon-triangle-left"></a></li>
		<c:forEach begin="1" end="${counts.page }" var="i">
			<li><a href="mainbbs.do?page=${i }">${i }</a>
		</c:forEach>
		<li><a href="#"><span class="glyphicon glyphicon-triangle-right"></a></li>
		 -->
		<li>
			<c:if test="${page > 5}">
				<a href="sortedbbs.do?sort=${sort }&page=${startgrouppage-1}">
					<span class="glyphicon glyphicon-triangle-left"></span>
				</a>
			</c:if> 
			<c:forEach begin="${startgrouppage}" end="${endgrouppage}" var="i" step="1">
				<c:if test="${i == page}">
					<a>${i}</a>
				</c:if>
				<c:if test="${i != page}">
					<a href="sortedbbs.do?sort=${sort }&page=${i}">${i}</a>	
				</c:if>
			</c:forEach> 
			<c:if test="${endgrouppage < counts.allpages}">
				<a href="sortedbbs.do?sort=${sort }&page=${endgrouppage+1}">
					<span class="glyphicon glyphicon-triangle-right"></span>
				</a>
			</c:if>
		</li>
	</ul>
</div>

