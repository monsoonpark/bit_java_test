<link href="https://fonts.googleapis.com/css?family=Cabin+Sketch|Londrina+Sketch" rel="stylesheet">
<style>
@import
	url(https://fonts.googleapis.com/css?family=Raleway:400,,800,900);

.titles .title {
	font-weight: 600;
	color: transparent;
	font-size: 100px;
	background:
		url("http://s1.1zoom.me/big0/149/432942-Kycb.jpg") repeat;
	-webkit-filter: brightness(0.9);	
	background-position: 70% 50%;
	-webkit-background-clip: text;
	position: relative;
	text-align: center;
	line-height: 150px;
	letter-spacing: -8px;
	font-family: 'Cabin Sketch', cursive;
}

.titles .subtitle {
	display: block;
	text-align: center;
	text-transform: uppercase;
	padding-top: 10px;
}
</style>
<div class="titles">
	<div class="container">
		<div class="title">Which is your favourite</div>
		<div class="subtitle"></div>
	</div>
</div>
<script>
	$(document).ready(function() {
		var mouseX, mouseY;
		var ww = $(window).width();
		var wh = $(window).height();
		var traX, traY;
		$(document).mousemove(function(e) {
			mouseX = e.pageX;
			mouseY = e.pageY;
			traX = ((6 * mouseX) / 70) + 50;
			traY = ((6 * mouseY) / 170) + 50;
			//console.log(traX);
			$(".title").css({
				"background-position" : traX + "%" + traY + "%"
			});
		});
	});
</script>