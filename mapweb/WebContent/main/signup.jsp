
<div class="container-fluid" id="signup">
	<div class="row main">
		<div class="main-login main-center">
			<h4>Sign up once and watch any of my bbs.</h4>
			<c:url var="uregister" value="/userregister.do"></c:url>
			<form class="signupform" method="post" action="${uregister }">		<!-- form  -->

				<div class="form-group">	
					<label for="name" class="cols-sm-2 control-label">ID</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"></span> 
							<input type="text" class="form-control" required="required"
								name="userid" placeholder="Enter your ID" />	<!-- input -->
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="username" class="cols-sm-2 control-label">Username</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"></span> 
							<input type="text" class="form-control" required="required"
								name="uname" placeholder="Enter your Username" />	<!-- input -->
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="password" class="cols-sm-2 control-label">Password</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"></span> 
							<input type="password" class="form-control" required="required"
								name="upw" id="password" placeholder="Enter your Password" />	<!-- input -->
						</div>
					</div>
				</div>
				<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>

				<div class="form-group ">
					<a class="btn btn-primary btn-lg btn-block login-button" 
					onclick="verify()">Register</a>	<!-- button -->
				</div>

			</form>
		</div>
	</div>
</div>
