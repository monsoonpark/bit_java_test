<div class="container">
	<ul>
		<li style="list-style: none;">
			<div class="col-sm-10 contact-form" id="insertmenu">
				<form action="contentmodify.do?num=${detail.bid }" method="post">
					<div class="row">
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="title" name="title" value="${detail.btitle }" type="text" readonly/>	<!-- 제목 -->
						</div>
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="name" name="name" value="${detail.uname }" readonly />	<!-- 작성자 -->
						</div>
					</div>
					
					<textarea class="form-control" id="content" name="content" rows="20" readonly>${detail.bcontent }</textarea>	<!-- 내용 -->
					
					<br />
					<div class="row">
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="location" name="location" 
							value="${detail.blocation }" type="text" required readonly/>	<!-- 위치 -->
						</div>
						<div class="col-xs-12 col-md-6 form-group">
							<div class="btn-group pull-right">
								<a class="btn btn-primary" onclick="back()"><span class="glyphicon glyphicon-menu-left"></span> 뒤로가기</a>	<!-- 버튼 -->
								<c:if test="${detail.ucode eq hm.ucode or hm.ucode eq 1}">
								<button class="btn btn-success"><span class="glyphicon glyphicon-wrench"></span> 수정하기</button>	<!-- 버튼 -->
								<a class="btn btn-danger" href="contentdelete.do?num=${detail.bid }"><span class="glyphicon glyphicon-trash"></span> 삭제하기</a>	<!-- 버튼 -->
								</c:if>
							</div>
						</div>
					</div>
					<input type="hidden" name="ucode" value="${detail.ucode }" readonly/>
					<input type="hidden" name="bsort" value="${detail.bcode }" readonly/>
					<input type="hidden" id="sort" name="sort" value="${detail.scode }"required readonly/>
					
				</form>
				<jsp:include page="/center/map.jsp" />
			</div>
		</li>
	</ul>
</div>