<div class="container">
	<ul>
		<li style="list-style: none;">
			<div class="col-sm-10 contact-form" id="insertmenu">
				<form action="contentupdate.do" method="post">
					<div class="row">
						<div class="col-xs-6 col-md-2 form-group">
							<!-- select -->
							<select class="form-control" data-style="btn-info"
								onchange="ssort(this.value)" id="ssort" name="ssort"
								style="width:80px;">
								<option value="">분류</option>
								<c:forEach items="${sortlist }" var="s">
									<c:choose>
										<c:when test="${s.scode eq detail.scode }">
											<option value="${s.scode }" selected>${s.sname }</option>
										</c:when>
										<c:otherwise>
											<option value="${s.scode }">${s.sname }</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="title" name="title" value="${title }"type="text" />
						</div>
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="name" name="name" value="${hm.uname }" readonly />
						</div>
					</div>
					
					<textarea class="form-control" id="content" name="content" rows="20" >${content }</textarea>
					
					<br />
					<div class="row">
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="location" name="location" 
							value="${location }" type="text" required readonly/>
						</div>
						<div class="col-xs-12 col-md-6 form-group">
							<div class="btn-group pull-right">
								<a class="btn btn-primary" onclick="back()">뒤로가기</a>	
								<button class="btn btn-success">수정완료</button>	<!-- ë²í¼ -->
								
							</div>
						</div>
						
					</div>
					<input type="hidden" name="ucode" value="${detail.ucode }" readonly/>
					<input type="hidden" name="bsort" value="${detail.bcode }" readonly/>
					<input type="hidden" id="sort" name="sort" value="1"required readonly/>
					<input type="hidden" id="num" name="num" value="${num }"required readonly/>
					
					
				</form>
				<jsp:include page="/center/map.jsp" />
			</div>
		</li>
	</ul>
</div>