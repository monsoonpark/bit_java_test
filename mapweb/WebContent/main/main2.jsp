<div class="container">
	<div class="tabbable tabs-left">
		<!-- 왼쪽과 오른쪽 탭에만 필요 -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1" data-toggle="tab">메인 페이지</a></li>
			<li><a href="bbslist.jsp">게시판</a></li>
			<li><a href="map.jsp">맵</a></li>
			<li><a href="test.jsp">테스트</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1">
					<h1>메인 페이지</h1>
					<div class="container">
						<div class="tabbable tabs-left">
							<!-- 왼쪽과 오른쪽 탭에만 필요 -->
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab01" data-toggle="tab">메인
										페이지</a></li>
								<li><a href="#tab02" data-toggle="tab">게시판</a></li>
								<li><a href="#tab03" data-toggle="tab">맵</a></li>
								<li><a href="#tab04" data-toggle="tab">테스트</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab01">
									TEST
								</div>
								<div class="tab-pane fade" id="tab02">
									<h1>게시판</h1>
									<jsp:include page="/center/bbslist.jsp" flush="true"/>
								</div>
								<div class="tab-pane fade" id="tab03">
									<h1>구글맵</h1>
									<jsp:include page="/center/map.jsp" flush="true"/>
								</div>
								<div class="tab-pane fade" id="tab04">
									<h1>테스트</h1>
									<jsp:include page="/center/test.jsp" flush="true"/>
								</div>
							</div>
						</div>
					</div>
			</div>
			<div class="tab-pane fade" id="tab2">
				
			</div>
			<div class="tab-pane fade" id="tab3">
				
			</div>
			<div class="tab-pane fade" id="tab4">
				
				
			</div>
		</div>
	</div>
</div>