<div class="container" style="margin:0px; width:100%;">
	<input type="hidden" id="bsort" name="bsort" value="1" readonly/>
	<div class="tabbable tabs-left">
		<!-- 왼쪽과 오른쪽 탭에만 필요 -->
		<ul class="nav nav-tabs">
			<li><a href="mainbbs.do" class="maintext"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<li class="active"><a href="#tab01" data-toggle="tab" class="maintext"><span class="glyphicon glyphicon-th-list"></span> 맛집 게시판</a></li>
			<li><a href="#tab03" data-toggle="tab" class="maintext"><span class="glyphicon glyphicon-globe"></span> 맛집 지도</a></li>
			<li><a href="#tab04" data-toggle="tab" class="maintext"><span class="glyphicon glyphicon-alert"></span> 테스트</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab01">	<!-- 게시판 -->
				<jsp:include page="/center/bbslist.jsp" />
			</div>
			
			<div class="tab-pane fade" id="tab03">	<!-- 구글맵 -->
				<h3 id="maptext">Tasty Road<small> ( click <span class="glyphicon glyphicon-pushpin"></span> to move)</small></h3>
				<div class="row">
					<div class="col-md-3 form-group">
								<input class="form-control" id="location" name="location" value=""
									placeholder="위치를 선택하세요" type="text" disabled/>	<!-- 위치 -->
					</div>
				</div>
				<jsp:include page="/center/map.jsp" />
			</div>
			
			<div class="tab-pane fade" id="tab04">	<!-- 테스트 -->
				<h1>테스트</h1>
				<jsp:include page="/center/test.jsp" />
			</div>
		</div>
	</div>
</div>