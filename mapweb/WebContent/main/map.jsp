<div class="container">
	<div class="tabbable tabs-left">
		<!-- 왼쪽과 오른쪽 탭에만 필요 -->
		<ul class="nav nav-tabs">
			<li><a href="main.jsp">메인 페이지</a></li>
			<li><a href="bbslist.jsp">게시판</a></li>
			<li class="active"><a href="#tab2" data-toggle="tab">맵</a></li>
			<li><a href="test.jsp">테스트</a></li>
		</ul>
		<div class="tab-content">

			<div class="tab-pane fade in active" id="tab3">

				<hr />
				<h3>Tasty Road</h3>
				<div id="map"></div>
			</div>
		</div>
	</div>
</div>

