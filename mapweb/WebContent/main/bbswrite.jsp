<div class="container">
	<ul>
		<li style="list-style: none;">
			<div class="col-sm-10 contact-form" id="insertmenu">
				<form id="writeform" method="post" class="form" role="form" action="bbsregister.do">
					<div class="row">
						<div class="col-xs-6 col-md-2 form-group">	<!-- select -->
							<select class="form-control" data-style="btn-info" 
								onchange="ssort(this.value)" id="ssort" name="ssort" style="width:80px;" >
								<option value="">분류</option>
								<c:forEach items="${sortlist }" var="s">
									<option value="${s.scode }">${s.sname }</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="title" name="title" placeholder="제목" type="text" required="required" autofocus />	<!-- 제목 -->
						</div>
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="name" name="name" value="${hm.uname }" required="required" readonly />	<!-- 작성자 -->
						</div>
					</div>
					
					<textarea class="form-control" id="content" name="content" placeholder="Message" rows="5"></textarea>	<!-- 내용 -->
					
					<br />
					<div class="row">
						<div class="col-xs-6 col-md-6 form-group">
							<input class="form-control" id="location" name="location" 
							placeholder="위치를 선택하세요" type="text" required readonly/>	<!-- 위치 -->
						</div>
						<div class="col-xs-12 col-md-6 form-group">
							<a class="btn btn-primary pull-right" onclick="writeform()">Submit</a>	<!-- 버튼 -->
						</div>
					</div>
					<input type="hidden" name="ucode" value="${hm.ucode }" readonly/>
					<input type="hidden" name="bsort" value="1" readonly/>
					<input type="hidden" id="sort" name="sort" required readonly/>
					
				</form>
				<jsp:include page="/center/map.jsp" />
			</div>
		</li>
	</ul>
</div>