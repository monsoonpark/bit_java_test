<%@tag import="the.test.Code"%>
<%@tag import="java.util.ArrayList"%>
<%@tag import="java.sql.ResultSet"%>
<%@tag import="the.bits.Bitdb"%>
<%@ tag body-content="empty" language="java" pageEncoding="UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@ attribute name="table" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	// select
	ResultSet rs = Bitdb.executeQuery("SELECT * FROM " + table);
	// ResultSet -> ArrayList<Code>
	ArrayList<Code> codes = new ArrayList<>();
	while (rs.next()) {
		codes.add(new Code(rs.getString(2), rs.getInt(1)));
	}
	// setAttribute
	request.setAttribute("codes", codes);
%>
<select name='${table}'>
	<c:forEach items="${codes}" var="c">
		<option value="${c.code}">${c.name}</option>
	</c:forEach>
</select>
