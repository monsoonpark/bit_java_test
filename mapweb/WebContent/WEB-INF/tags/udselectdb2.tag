<%@tag import="the.test.Code"%>
<%@ tag body-content="empty" language="java" pageEncoding="UTF-8"
	trimDirectiveWhitespaces="true"%>
<%@tag import="java.util.ArrayList"%>
<%@tag import="java.sql.ResultSet"%>
<%@ tag import="the.bits.Bitdb"%>
<%@ attribute name="tablename" required="true"%>
<%@ attribute name="listname" type="java.lang.String"
	rtexprvalue="false" required="true"%>
<%@ variable name-from-attribute="listname" alias="result"
	variable-class="java.lang.String" scope="AT_END"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	// select
	ResultSet rs = Bitdb.executeQuery("SELECT * FROM " + tablename);
	// ResultSet -> ArrayList<Code>
	ArrayList<Code> codes = new ArrayList<>();
	// 태그 만들기
	StringBuffer sb = new StringBuffer();
	sb.append(String.format("<select name='%s'>", tablename));
	while (rs.next()) {
		sb.append(String.format("<option value='%s'>%s</option>", rs.getString(1), rs.getString(2)));
	}
	sb.append("</select>");
%>
<c:set var="result" value="<%=sb.toString()%>"></c:set>

