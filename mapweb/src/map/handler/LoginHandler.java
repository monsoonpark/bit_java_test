package map.handler;

import java.util.HashMap;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class LoginHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		//파라미터
		
		// 초기
		req.setCharacterEncoding("utf-8");
		String userid=req.getParameter("userid");
		String upw=req.getParameter("upw");
		HashMap<String, Object> hm=null;
		System.out.println(userid);
		System.out.println(upw);
		// null인지 확인
		if (Objects.isNull(userid) || userid.equals("") || Objects.isNull(upw) || upw.equals("")){
			System.out.println("값이 없습니다.");
		} else {
			// 아이디 비밀번호 정상입력
			if ((!Objects.isNull(hm = (Mapdao.getInstance()).checkLogin(userid, upw)))) {
				// 로그인
				HttpSession session = req.getSession();
				session.setAttribute("hm", hm);
				session.setAttribute("login", "ok");
			}

		}
				
		return "mainbbs.do";
	}

}
