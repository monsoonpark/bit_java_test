package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import the.bits.Bitdb;

public class UserRegisterHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setCharacterEncoding("utf-8");
		String sql=String.format("insert into bbs_user(ucode,uname,userid,upw) "
				+ "values(seq_bbs_user.nextval,'%s','%s','%s')",
				req.getParameter("uname"),req.getParameter("userid"),req.getParameter("upw"));
		
		Bitdb.execute(sql);
		
		return "mainbbs.do";
	}

}
