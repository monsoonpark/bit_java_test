package map.handler;

import java.util.HashMap;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class MainBbsHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setCharacterEncoding("utf-8");
		Mapdao mapdao=Mapdao.getInstance();
		
		HttpSession session = req.getSession();
		session.setAttribute("sort", 0);
		req.setAttribute("bsort", 1);
		String page=req.getParameter("page");
		if(Objects.isNull(page) || page.equals("")){
			req.setAttribute("maplist", mapdao.mapselect(1));
			page="1";
		}else {
			int pagenum=Integer.parseInt(req.getParameter("page"));
			req.setAttribute("maplist", mapdao.mapselect(pagenum));
		}
		req.setAttribute("maplocate", mapdao.mapselect());
		req.setAttribute("sortlist", mapdao.ssort(1));
		req.setAttribute("counts", mapdao.countPage());
		
		req.setAttribute("page", page);
		// 페이지그룹개수
		double grouppage = 5;
		req.setAttribute("grouppage", (int) grouppage);
		HashMap<String, Integer> map = (HashMap<String, Integer>) req.getAttribute("counts");
		int allpages = map.get("page");
		// 시작페이지번호
		int startgrouppage = ((int) Math.floor((Integer.parseInt(page) - 1) / grouppage)) * (int) grouppage + 1;
		req.setAttribute("startgrouppage", startgrouppage);
		// 종료페이지번호
		int endgrouppage = startgrouppage + ((int) grouppage - 1);
		endgrouppage = endgrouppage > allpages ? allpages : endgrouppage;
		req.setAttribute("endgrouppage", endgrouppage);
		
		return "/main/main.jsp";
	}

}
