package map.handler;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import the.bits.Bitdb;

public class UserUpdateHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setCharacterEncoding("utf-8");
		String sql;
		
		if(req.getParameter("uname").equals("") || Objects.isNull(req.getParameter("uname"))) {
			return "mainbbs.do";
		}
		
		if(req.getParameter("upw").equals("") || Objects.isNull(req.getParameter("upw"))) {
			sql=String.format("update bbs_user set uname='%s' where ucode=%s ",
					req.getParameter("uname"),req.getParameter("ucode"));
			
			System.out.println("login.do?userid="+req.getParameter("userid")+"&upw="+req.getParameter("upw2"));
			Bitdb.execute(sql);
			return String.format("login.do?userid=%s&upw=%s", req.getParameter("userid"),req.getParameter("upw2"));
		}else {
			sql=String.format("update bbs_user set uname='%s',upw='%s' where ucode=%s ",
					req.getParameter("uname"),req.getParameter("upw"),req.getParameter("ucode"));
			req.setAttribute("login", "changed");
		}
		
		Bitdb.execute(sql);
		System.out.println(req.getParameter("uname"));
		System.out.println(req.getParameter("upw"));
		System.out.println(req.getParameter("ucode"));
		
		return "mainbbs.do";
	}

}
