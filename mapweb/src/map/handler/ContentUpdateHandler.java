package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class ContentUpdateHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setCharacterEncoding("utf-8");
		int num=Integer.parseInt(req.getParameter("num"));
		int sort=Integer.parseInt(req.getParameter("sort"));
		String title=req.getParameter("title");
		String content=req.getParameter("content");
		String location=req.getParameter("location");
		(Mapdao.getInstance()).contentUpdate(num, title, sort, content, location);
		return "contentdetail.do?num="+num;
	}

}
