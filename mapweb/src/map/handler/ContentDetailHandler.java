package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import map.command.CommandHandler;
import map.dao.Mapdao;
import the.bits.Bitdb;

public class ContentDetailHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		HttpSession session = req.getSession();
		int num=Integer.parseInt(req.getParameter("num"));
		session.setAttribute("bsort", 0);
		req.setAttribute("detail", (Mapdao.getInstance()).contentDetail(num));
		
		return "/main/contentdetail.jsp";
	}

}
