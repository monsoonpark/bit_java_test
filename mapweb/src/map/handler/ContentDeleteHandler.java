package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class ContentDeleteHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		int num=Integer.parseInt(req.getParameter("num"));
		(Mapdao.getInstance()).contentDelete(num);
		return "/mainbbs.do";
	}

}
