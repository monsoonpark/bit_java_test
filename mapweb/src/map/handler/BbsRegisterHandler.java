package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class BbsRegisterHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setCharacterEncoding("utf-8");
		/*
		req.getParameter("bsort");
		req.getParameter("sort");
		req.getParameter("title");
		req.getParameter("name");
		req.getParameter("ucode");
		req.getParameter("content");
		req.getParameter("location");
		*/
		System.out.println("게시판분류: "+req.getParameter("bsort"));
		System.out.println("글분류: "+req.getParameter("sort"));
		System.out.println("제목: "+req.getParameter("title"));
		System.out.println("작성자: "+req.getParameter("name"));
		System.out.println("작성자코드: "+req.getParameter("ucode"));
		System.out.println("내용: "+req.getParameter("content"));
		System.out.println("위치: "+req.getParameter("location"));
		(Mapdao.getInstance()).insertMap(req.getParameter("bsort"),
				req.getParameter("sort"),
				req.getParameter("title"), 
				req.getParameter("name"),
				req.getParameter("ucode"),
				req.getParameter("content"),
				req.getParameter("location"));
		
		return "mainbbs.do";
	}

}
