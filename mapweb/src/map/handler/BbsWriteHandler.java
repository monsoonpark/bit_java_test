package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class BbsWriteHandler implements CommandHandler {

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// TODO Auto-generated method stub
		req.setAttribute("sortlist", (Mapdao.getInstance()).ssort(1));
		req.setAttribute("bsort", 0);
		return "/main/bbswrite.jsp";
	}

}
