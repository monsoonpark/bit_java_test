package map.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import map.command.CommandHandler;
import map.dao.Mapdao;

public class UserModifyHandler implements CommandHandler{

	@Override
	public String process(HttpServletRequest req, HttpServletResponse res) throws Exception {
		req.setCharacterEncoding("utf-8");
		/*
		int num=Integer.parseInt(req.getParameter("num"));
		String title=req.getParameter("title");
		String content=req.getParameter("content");
		String location=req.getParameter("location");
		req.setAttribute("num", num);
		req.setAttribute("title", title);
		req.setAttribute("content", content);
		req.setAttribute("location", location);
		req.setAttribute("sortlist", (Mapdao.getInstance()).ssort(1));
		req.setAttribute("detail", (Mapdao.getInstance()).contentDetail(num));
		*/
		
		return "/main/userupdate.jsp";
	}

}
