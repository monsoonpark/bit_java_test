package map.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import the.bits.Bitdb;

public class Mapdao {
	static Mapdao dao;
	static {
		dao = new Mapdao();
	}

	// 생성자
	private Mapdao() {
	}

	// getInstance()
	public static Mapdao getInstance() {
		return dao;
	}
	
	//해당글 select (num=bid)
	public HashMap<String, Object> contentDetail(int num){
		String sql=String.format("select * from v_bbs_map where bid=%s", num);
		ResultSet rs=Bitdb.executeQuery(sql);
		HashMap<String, Object> detail=null;
		String location=null;
		String[] lo=new String[2];
		try {
			while(rs.next()) {
				detail = new HashMap<>();
				detail.put("bid", rs.getInt("bid"));
				detail.put("bcode", rs.getInt("bcode"));
				detail.put("scode", rs.getInt("scode"));
				detail.put("ucode", rs.getString("ucode"));
				detail.put("btitle", rs.getString("btitle"));
				detail.put("bcontent", rs.getString("bcontent"));
				detail.put("bdate", rs.getString("bdate"));
				detail.put("bgroup", rs.getInt("bgroup"));
				detail.put("bverti", rs.getInt("bverti"));
				detail.put("bmargin", rs.getInt("bmargin"));
				detail.put("sname", rs.getString("sname"));
				detail.put("uname", rs.getString("uname"));
				
				location=rs.getString("blocation");
				detail.put("blocation", location);
				if((!location.equals("")) && !(Objects.isNull(location)) ) {
					location=location.substring(1, location.indexOf(")"));
					detail.put("location", location);
					
					lo = location.split(", ");
					detail.put("lat", lo[0]);
					detail.put("lng", lo[1]);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return detail;
	}
	
	//해당글 삭제
	public void contentDelete(int num) {
		String sql=String.format("delete bbs_map where bid=%s",num);
		Bitdb.execute(sql);
		System.out.println(num+"번 글 삭제");
	}
	
	//글 수정
	public void contentUpdate(int num, String title, int sort, String content, String location) {
		String sql=String.format("update bbs_map set btitle='%s', scode=%s, bcontent='%s', blocation='%s' where bid=%s", 
				title,sort,content,location,num);
		Bitdb.execute(sql);
		System.out.println(num+"번 글 수정");
	}
	
	

	//게시글 db
	public ArrayList<HashMap<String, Object>> mapselect(int page,int scode) {
		/*
		String sql="select m.*, s.sname, u.uname "
				+ " from bbs_map m, bbs_ssort s, bbs_user u "
				+ " where s.scode=m.scode and u.ucode=m.ucode "
				+ " order by bgroup desc, bverti asc ";
		*/
		String sql=String.format(" select * from (SELECT * FROM v_bbs_map WHERE scode=%s) where FNC_TEST_PAGING(R,%s,10)=1",scode, page);
		ResultSet rs = Bitdb.executeQuery(sql);
		String location = null;
		String lat = null;
		String lng = null;
		String[] lo = { lat, lng };
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<>();
		HashMap<String, Object> hm = null;
		try {
			while (rs.next()) {
				hm = new HashMap<>();
				hm.put("bid", rs.getInt("bid"));
				hm.put("bcode", rs.getInt("bcode"));
				hm.put("scode", rs.getInt("scode"));
				hm.put("ucode", rs.getString("ucode"));
				hm.put("btitle", rs.getString("btitle"));
				hm.put("bcontent", rs.getString("bcontent"));
				hm.put("bdate", rs.getString("bdate"));
				hm.put("bgroup", rs.getInt("bgroup"));
				hm.put("bverti", rs.getInt("bverti"));
				hm.put("bmargin", rs.getInt("bmargin"));
				hm.put("sname", rs.getString("sname"));
				hm.put("uname", rs.getString("uname"));
				
				location=rs.getString("blocation");
				hm.put("blocation", location);
				if((!location.equals("")) && !(Objects.isNull(location)) ) {
					location=location.substring(1, location.indexOf(")"));
					hm.put("location", location);
					
					lo = location.split(", ");
					hm.put("lat", lo[0]);
					hm.put("lng", lo[1]);
				}
				maplist.add(hm);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch blockServlet06_Seoul
			e.printStackTrace();
		}finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maplist;
	}
	//게시글 db
	public ArrayList<HashMap<String, Object>> mapselect(int page) {
		/*
		String sql="select m.*, s.sname, u.uname "
				+ " from bbs_map m, bbs_ssort s, bbs_user u "
				+ " where s.scode=m.scode and u.ucode=m.ucode "
				+ " order by bgroup desc, bverti asc ";
		*/
		String sql=String.format("SELECT * FROM v_bbs_map WHERE FNC_TEST_PAGING(R,%s,10)=1", page);
		ResultSet rs = Bitdb.executeQuery(sql);
		String location = null;
		String lat = null;
		String lng = null;
		String[] lo = { lat, lng };
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<>();
		HashMap<String, Object> hm = null;
		try {
			while (rs.next()) {
				hm = new HashMap<>();
				hm.put("bid", rs.getInt("bid"));
				hm.put("bcode", rs.getInt("bcode"));
				hm.put("scode", rs.getInt("scode"));
				hm.put("ucode", rs.getString("ucode"));
				hm.put("btitle", rs.getString("btitle"));
				hm.put("bcontent", rs.getString("bcontent"));
				hm.put("bdate", rs.getString("bdate"));
				hm.put("bgroup", rs.getInt("bgroup"));
				hm.put("bverti", rs.getInt("bverti"));
				hm.put("bmargin", rs.getInt("bmargin"));
				hm.put("sname", rs.getString("sname"));
				hm.put("uname", rs.getString("uname"));
				
				location=rs.getString("blocation");
				hm.put("blocation", location);
				if((!location.equals("")) && !(Objects.isNull(location)) ) {
					location=location.substring(1, location.indexOf(")"));
					hm.put("location", location);
					
					lo = location.split(", ");
					hm.put("lat", lo[0]);
					hm.put("lng", lo[1]);
				}
				maplist.add(hm);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch blockServlet06_Seoul
			e.printStackTrace();
		}finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return maplist;
	}
	//게시글 db
	public ArrayList<HashMap<String, Object>> mapselect() {

		String sql = "select * from v_bbs_map";

		// String sql=String.format("SELECT * FROM v_bbs_map WHERE
		// FNC_TEST_PAGING(R,%s,10)=1", page);
		ResultSet rs = Bitdb.executeQuery(sql);
		String location = null;
		String lat = null;
		String lng = null;
		String[] lo = { lat, lng };
		ArrayList<HashMap<String, Object>> maplist = new ArrayList<>();
		HashMap<String, Object> hm = null;
		try {
			while (rs.next()) {
				hm = new HashMap<>();
				hm.put("bid", rs.getInt("bid"));
				hm.put("bcode", rs.getInt("bcode"));
				hm.put("scode", rs.getInt("scode"));
				hm.put("ucode", rs.getString("ucode"));
				hm.put("btitle", rs.getString("btitle"));
				hm.put("bcontent", rs.getString("bcontent"));
				hm.put("bdate", rs.getString("bdate"));
				hm.put("bgroup", rs.getInt("bgroup"));
				hm.put("bverti", rs.getInt("bverti"));
				hm.put("bmargin", rs.getInt("bmargin"));
				hm.put("sname", rs.getString("sname"));
				hm.put("uname", rs.getString("uname"));

				location = rs.getString("blocation");
				hm.put("blocation", location);
				location = location.substring(1, location.indexOf(")"));
				hm.put("location", location);
				// System.out.println(location);
				lo = location.split(", ");
				hm.put("lat", lo[0]);
				hm.put("lng", lo[1]);
				maplist.add(hm);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch blockServlet06_Seoul
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return maplist;
	}
	
	//글분류
	public ArrayList<HashMap<String, Object>> ssort(int i) {
		ResultSet rs = Bitdb.executeQuery(String.format("select * from bbs_ssort where bcode=%s", i));
		ArrayList<HashMap<String, Object>> sortlist = new ArrayList<>();
		HashMap<String, Object> hm = null;
		try {
			while (rs.next()) {
				hm = new HashMap<>();
				hm.put("scode", rs.getInt("scode"));
				hm.put("sname",rs.getString("sname"));
				sortlist.add(hm);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return sortlist;
	}
	//페이지
	public HashMap<String, Integer> countPage() {
		String sql="SELECT COUNT(*) allcounts, CEIL(COUNT(*)/10) allpages FROM v_bbs_map";
		ResultSet rs=Bitdb.executeQuery(sql);
		HashMap<String, Integer> page=null;
		
		try {
			while(rs.next()) {
				page=new HashMap<>();
				page.put("page", rs.getInt("allpages"));
				page.put("count", rs.getInt("allcounts"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return page;
	}
	//페이지
		public HashMap<String, Integer> countPage(int scode) {
			String sql=String.format("SELECT COUNT(*) allcounts, CEIL(COUNT(*)/10) allpages FROM v_bbs_map where scode=%s",scode);
			ResultSet rs=Bitdb.executeQuery(sql);
			HashMap<String, Integer> page=null;
			
			try {
				while(rs.next()) {
					page=new HashMap<>();
					page.put("page", rs.getInt("allpages"));
					page.put("count", rs.getInt("allcounts"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					Bitdb.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			return page;
		}
	
	//테스트 글분류
	public ArrayList<HashMap<String, Object>> testsort(int i) {
		String sql=(String.format("select * from bbs_ssort where bcode=%s", i));
		ArrayList<HashMap<String, Object>> sortlist=Bitdb.makeListMap(sql);

		return sortlist;
	}
	
	//로그인 체크
	public HashMap<String, Object> checkLogin(String userid, String upw) {
		String sql = String.format("select * from bbs_user where userid='%s' and upw='%s'", userid,upw);
		HashMap<String, Object> hm=new HashMap<>();
		ResultSet rs = Bitdb.executeQuery(sql);
		try {
			if (rs.next()) {
				hm.put("ucode", rs.getInt("ucode"));
				hm.put("uname", rs.getString("uname"));
				hm.put("userid", rs.getString("userid"));
				hm.put("upw", rs.getString("upw"));
				return hm;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	//insert
	public String insertMap(String bsort, String sort,String title, String name,String ucode, String content, String location) {
		if(sort.equals("") || Objects.isNull(sort)) {
			sort="4";
		}
		int bs=Integer.parseInt(bsort);
		int s=Integer.parseInt(sort);
		int u=Integer.parseInt(ucode);
		String sql=String.format("insert into bbs_map(bid,bcode,scode,ucode,btitle,bcontent,blocation) "
				+ "values(seq_bbs_map.nextval,%s,%s,%s,'%s','%s','%s')",
				bs,s,u,title,content,location);
		Bitdb.execute(sql);
			
		return "mainbbs.do";
	}
	

	//테스트용 맵
	public ArrayList<HashMap<String, String>> maplist() {
		// 초기
		ArrayList<HashMap<String, String>> list = new ArrayList<>();
		HashMap<String, String> hm = null;
		// select
		ResultSet rs = Bitdb.executeQuery("select * from test_map");
		// rs=>ArrayList<HashMap<String,String>>
		try {
			ResultSetMetaData md = rs.getMetaData();
			while (rs.next()) {
				hm = new HashMap<>();
				for (int i = 1; i < md.getColumnCount(); i++) {
					hm.put(md.getColumnName(i), rs.getString(i));
				}
				list.add(hm);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				Bitdb.close();
			} catch (Exception e) {

			}
		}
		return list;
	}
}
