package map.service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import the.bits.Bitdb;

public class TestService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request,response);
	}
	
	private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		/*
		ResultSet rs= Bitdb.executeQuery("select * from test_map");
		String location=null;
		String lat=null;
		String lng=null;
		String[] lo= {lat, lng};
		ArrayList<HashMap<String,Object>> loc=new ArrayList<>();
		HashMap<String,Object> hm=null;
		try {
			while(rs.next()){
				hm=new HashMap<>();
				hm.put("mid", rs.getInt("mid"));
				hm.put("mtitle",rs.getString("mtitle"));
				hm.put("mcontent",rs.getString("mcontent"));
				hm.put("muserid",rs.getString("userid"));
				hm.put("mdate",rs.getString("mdate"));
				location=rs.getString("mlocation").substring(1, rs.getString("mlocation").indexOf(")"));
				//System.out.println(location);
				lo=location.split(", ");
				hm.put("lat", lo[0]);
				hm.put("lng", lo[1]);
				loc.add(hm);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch blockServlet06_Seoul
			e.printStackTrace();
		}
		request.setAttribute("location", loc);
		*/
		//request.getRequestDispatcher("mainbbs.do").forward(request, response);
		
		response.sendRedirect("mainbbs.do");
		//request.getRequestDispatcher("mainbbs.do").forward(request, response);
	}

}
