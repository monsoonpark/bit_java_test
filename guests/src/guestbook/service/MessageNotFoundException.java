package guestbook.service;

import javax.management.ServiceNotFoundException;

public class MessageNotFoundException extends ServiceException {
	public MessageNotFoundException(String message) {
		super(message);
	}
}
